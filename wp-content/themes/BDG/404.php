<?php get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); ?>

<script>
  // setTimeout(function(){location.href="/"} , 8000);
</script>
<div class="container">
	<div class="col-lr-0 col-lg-12 col-md-12 col-sm-2 col-xs-12">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 error-404">
			ERROR 404
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			Strona, której szukasz nie istnieje.
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			Zaraz zostaniesz przekierowany na stronę główną.
		</div>
	</div>
</div>

<?php get_footer(); ?>