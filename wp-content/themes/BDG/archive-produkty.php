<?php
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); 
?>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/produkty.jpg');">
	<div class="caption-over-block-all">
		<div class="caption-over-outer-all">
			<div class="caption-over-inner-all top-banner-padding">
				<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
					<h1>Produkty</h1>
					<img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down-white.png" class="img-responsive top-banner-twig">
				</div>
			</div>
		</div>
	</div>
</div>


<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 products-home" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.jpg');">
	<div class="container">
	
	<?php $terms = get_terms('produkty-category',array('hide_empty' => false)); ?>
	<?php foreach ( $terms as $term ) { ?>
		<a href="/produkty/<?php echo $term->slug; ?>" title="<?php echo $term->name; ?>">
			<div class="col-lr-0 col-lg-3 col-md-4 col-sm-6 col-xs-12 h-lg-3 h-md-4 h-sm-6 h-xs-12">
				<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 category-box" style="background-image: url('<?php echo get_field('miniaturka_na_stronie_glownej','term_' . $term->term_id); ?>');">
					<div id="parent-<?php the_ID(); ?>" class="parent-page center h-content">
						<div class="caption-over-block-all">
							<div class="caption-over-outer-all">
								<div class="caption-over-inner-all">
									<h1><?php echo $term->name; ?></h1>
									<img src="<?php echo get_template_directory_uri(); ?>/img/twig-products.png" class="img-responsive twig-products">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</a>
	<?php 
	}
	wp_reset_query(); 
	?>
	</div>
</div>


<?php get_footer(); ?>