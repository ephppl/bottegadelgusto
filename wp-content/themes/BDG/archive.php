<?php get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); ?>

<div class="container">				
	<?php while(have_posts()) : the_post(); ?>		
		<a href="<?php the_permalink(); ?>">		
			<div class="col-lg-12 col-md-12 col-xs-12">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?> </a>
			</div>	
		</a>
		<?php the_excerpt(); ?>				
	<?php endwhile; ?>
		
	<?php wpbeginner_numeric_posts_nav(); ?>
	<?php wp_reset_query(); ?>
</div>

<?php get_footer(); ?>