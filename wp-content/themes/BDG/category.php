<?php 
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); ?>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('/wp-content/uploads/2017/09/aktualnosci-background.jpg');">
	<div class="caption-over-block-all">
		<div class="caption-over-outer-all">
			<div class="caption-over-inner-all top-banner-padding">
				<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
					<h1><?php single_cat_title(); ?></h1>
					<img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down-white.png" class="img-responsive top-banner-twig">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 recipes-home-przepisy" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.jpg');">
	<div class="col-lr-0 container background-white">
		<div class="container">
			<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:30px 0px;">
				<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php
/*
					@$wp_query = new WP_Query(array(
					 'post_type' => 'post', //TUTAJ WPISZ NAZWĘ CPT
					 'showposts' => -1,
					 'orderby' => 'date', //RODZAJ SORTOWANIA (PO CZYM SOTRUJESZ?)
					 'order' => 'DESC' // DESC - OD NAJNOWSZEJ DO NAJSTARSZEJ ; ASC - OD NAJSTARSZEJ DO NAJNOWSZEJ (JAK SORTUJESZ?)
					));
*/
?>
					<?php 
						$i = 1;
						while (have_posts()){ the_post(); 
							$grafikaaktualnosci = get_field('grafika'); 
							$trescaktualnosci = get_the_content(); 
							$trimmed_content = wp_trim_words( $trescaktualnosci, 30, '' );
							$tytułaktualnosci = get_the_title();
							?>
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 aktualnosci-box">
									<?php if(!empty($grafikaaktualnosci)) {?>
										<a href="<?php the_permalink(); ?>"><div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 aktualnosci-img" style="background-image: url('<?php echo $grafikaaktualnosci; ?>');">
										</div></a>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<span class="aktualnosci-title"><?php echo $tytułaktualnosci;?></span>
											<span class="aktualnosci-content"><?php echo $trimmed_content; ?></span>
										</div>
										<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 aktualnosci-more">
											<a href="<?php the_permalink(); ?>">
												Więcej »
											</a>
										</div>
									<?php } ?>
								</div>
								<?php if($i / 3 == 0 ){ ?>
									<div class="row">
										<div class="col-lg-12 col-md-12 hidden-sm hidden-xs"></div>
									</div>
								<?php } ?>
							<?php $i++;
						} 						
					?>
				
				</div>
			</div>
		</div>
	</div>
</div>
					
<?php get_footer(); ?>