<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 trail" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/slider_trail.png');"></div>
	
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 tomatoes-footer">
	<img alt="tomatoes" src="<?php echo get_template_directory_uri(); ?>/img/tomatoes.png" class="img-responsive" />
</div>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 mini-header-logo mini-footer-logo">
	<div class="container">
		<a href="/"><img alt="logo" src="<?php echo get_template_directory_uri(); ?>/img/bottega-del-gusto.svg" class="img-responsive" /></a>
	</div>
</div>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="col-lr-0 container footer-contact">
		<div class="col-lr-0 col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12">
			<img alt="twig-adress" src="<?php echo get_template_directory_uri(); ?>/img/twig-adress.svg" class="img-responsive twig-adress" />
		</div>
		<div class="col-lr-0 col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12">

			<div class="col-lr-0 col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<?php echo get_field('adres',2); ?>
			</div>
			<div class="col-lr-0 col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<li><a href="tel:<?php echo str_replace("+","",str_replace(" ","",get_field('numer_telefonu',2))); ?>" target="_blank"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo get_field('numer_telefonu',2); ?></a></li>
				<li><a href="mailto:<?php echo get_field('e-mail',2); ?>" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo get_field('e-mail',2); ?></a></li>
				<li><a href="https://pl-pl.facebook.com/<?php echo get_field('facebook',2); ?>" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i> <?php echo get_field('facebook',2); ?></a></li>
			</div>
		</div>
		
		<div class="col-lr-0 col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12">
			<div class="col-lr-0 col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<?php echo get_field('godziny_otwarcia',2); ?>
			</div>
			<div class="col-lr-0 col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<?php wp_nav_menu(array(
					'container_class' => 'menu-header',
					'theme_location' => 'stopka',
					'items_wrap' => '%3$s',
					'deptah' => '1',
					'walker' => new BS3_Walker_Nav_Menu
				)); ?>
			</div>
		</div>
	</div>
</div>

<footer class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 footer-box">
		<div class="container">
			<div class="col-lr-0 col-lg-6 col-md-6 col-sm-6 col-xs-12 copyright-footer">
				© Copyright 2017
			</div>
			<div class="col-lr-0 col-lg-6 col-md-6 col-sm-6 col-xs-12 realization-footer">
				Realizacja: <a href="http://www.y-c.com.pl/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/yc-logo-white.svg" alt="Agencja YC"></a>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/cookies.js"></script>

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 834920283;
var google_conversion_label = "2mRYCJ7DqnUQ276PjgM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/834920283/?value=1.00&amp;label=2mRYCJ7DqnUQ276PjgM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</body>
</html>