<?php

/**********
INICJOWANIE
***********/

add_filter( 'show_admin_bar', '__return_false');

register_nav_menus( array(  
		'primary_1' => __( 'Menu 1', 'menu' ),
		'primary_2' => __( 'Menu 2', 'menu' ),
		'stopka' => __( 'Stopka menu', 'menu' )
	) 
);

add_filter('nav_menu_link_attributes', 'nav_link_att', 10, 3);

function nav_link_att($atts, $item, $args) {
	if ( $args->has_children )
	{
		//$atts['data-toggle'] = 'dropdown';
		$atts['class'] = 'dropdown-toggle';
	}
	return $atts;
}

// add something like this to functions.php
function fredy_custom_excerpt($text) {
  $text = strip_shortcodes( $text );
  $text = apply_filters('the_content', $text);
  $text = str_replace(']]>', ']]>', $text);
  $excerpt_length = apply_filters('excerpt_length', 55);
  $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
  return wp_trim_words( $text, $excerpt_length, $excerpt_more );
}

function change_wp_search_size($query) {
    if ( $query->is_search ) // Make sure it is a search page
        $query->query_vars['posts_per_page'] = 101; // Change 10 to the number of posts you would like to show

    return $query; // Return our modified query variables
}
add_filter('pre_get_posts', 'change_wp_search_size'); // Hook our custom function onto the request filter

/**********
CUSTOM LANGUAGE
***********/

function wpmllang() {

  if ( function_exists( 'icl_get_languages' ) ) {

    $existing_languages = icl_get_languages('skip_missing=0&orderby=KEY&order=DIR');

    foreach( $existing_languages as $lang ) {

      $extra_class = '';

      if( ICL_LANGUAGE_CODE == $lang['language_code'] ) {

          $extra_class = '';
      }

      printf( '
			  <a class="lang" href="%s">%s</a>
              ',
              $lang['url'],
              $lang['native_name']
            );
    }
  }
}



/************************
PAGINACJA STRON CATEGORII
************************/

function wpbeginner_numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div style="text-align:center;"><ul class="pagination">' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() ) {
		$npl_url = explode('"',get_previous_posts_link()); 
		echo '<li><a href="'.$npl_url[1].'" class="arrow-back-pagi">«</a></li>';
	}
	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li><span class="kropki">…</span></li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li><span class="kropki">…</span></li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() ) {
		$npl_url = explode('"',get_next_posts_link()); 
		echo '<li><a href="'.$npl_url[1].'" class="arrow-next-pagi">»</a></li>';
	}
	echo '</ul></div>' . "\n";

}



/*************************
DODAJ KLASE ACTIVE DO MENU
*************************/

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
     if( in_array('current-menu-item', $classes) ){
             $classes[] = 'active ';
     }
     return $classes;
}

function add_custom_classes($classes, $item){
	//var_dump(get_the_category($item->ID));
	@$categories = get_the_category( $post->ID );
	@$x = $categories[0]-> term_id;
	
    if(is_single() && $item->title == 'Promocje i wydarzenia' && $x == 1){
         $classes[] = 'active';
    }
	if(is_single() && $item->title == 'Promocje i wydarzenia' && $x == 4){
         $classes[] = 'active';
    }
	if(is_single() && is_singular('sklepy') && $item->title == 'Nasze sklepy'){
         $classes[] = 'active';
    }
    return $classes;
}

add_filter('nav_menu_css_class' , 'add_custom_classes' , 10 , 2);



/***********
FIRST & LAST 
***********/

function add_first_and_last($output) {
  $output = preg_replace('/class="menu-item/', 'class="first menu-item', $output, 1);
  $output = substr_replace($output, 'class="last menu-item', strripos($output, 'class="menu-item'), strlen('class="menu-item'));
  return $output;
}
add_filter('wp_nav_menu', 'add_first_and_last');



/************************
BREADCRUMPS
************************/

function blix_breadcrumbs() {
 
    /* === OPTIONS === */
    $text['home']     = 'Strona główna'; // text for the 'Home' link
    $text['category'] = 'Archive by Category "%s"'; // text for a category page
    $text['search']   = 'Search Results for "%s" Query'; // text for a search results page
    $text['tag']      = 'Posts Tagged "%s"'; // text for a tag page
    $text['author']   = 'Articles Posted by %s'; // text for an author page
    $text['404']      = 'Error 404'; // text for the 404 page
 
    $show_current   = 1; // 1 - show current post/page/category title in breadcrumbs, 0 - don't show
    $show_on_home   = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
    $show_home_link = 1; // 1 - show the 'Home' link, 0 - don't show
    $show_title     = 1; // 1 - show the title for the links, 0 - don't show
    $delimiter      = ' &raquo; '; // delimiter between crumbs
    $before         = '<span class="current">'; // tag before the current crumb
    $after          = '</span>'; // tag after the current crumb
    /* === END OF OPTIONS === */
 
    global $post;
    $home_link    = home_url('/');
    $link_before  = '<span typeof="v:Breadcrumb">';
    $link_after   = '</span>';
    $link_attr    = ' rel="v:url" property="v:title"';
    $link         = $link_before . '<a' . $link_attr . ' href="%1$s">%2$s</a>' . $link_after;
    $parent_id    = $parent_id_2 = $post->post_parent;
    $frontpage_id = get_option('page_on_front');
 
    if (is_home() || is_front_page()) {
 
        if ($show_on_home == 1) echo '<div class="breadcrumb"><a href="' . $home_link . '">' . $text['home'] . '</a></div>';
 
    } else {
 
        echo '<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">';
        if ($show_home_link == 1) {
            echo '<a href="' . $home_link . '" rel="v:url" property="v:title">' . $text['home'] . '</a>';
            if ($frontpage_id == 0 || $parent_id != $frontpage_id) echo $delimiter;
        }
 
        if ( is_category() ) {
            $this_cat = get_category(get_query_var('cat'), false);
            if ($this_cat->parent != 0) {
                $cats = get_category_parents($this_cat->parent, TRUE, $delimiter);
                if ($show_current == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
                $cats = str_replace('</a>', '</a>' . $link_after, $cats);
                if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
                echo $cats;
            }
            if ($show_current == 1) echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;
 
        } elseif ( is_search() ) {
            echo $before . sprintf($text['search'], get_search_query()) . $after;
 
        } elseif ( is_day() ) {
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
            echo $before . get_the_time('d') . $after;
 
        } elseif ( is_month() ) {
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            echo $before . get_the_time('F') . $after;
 
        } elseif ( is_year() ) {
            echo $before . get_the_time('Y') . $after;
 
        } elseif ( is_single() && !is_attachment() ) {
            if ( get_post_type() != 'post' ) {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                printf($link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
                if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;
            } else {
                $cat = get_the_category(); $cat = $cat[0];
                $cats = get_category_parents($cat, TRUE, $delimiter);
                if ($show_current == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
                $cats = str_replace('</a>', '</a>' . $link_after, $cats);
                if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
                echo $cats;
                if ($show_current == 1) echo $before . get_the_title() . $after;
            }
 
        } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
            $post_type = get_post_type_object(get_post_type());
            echo $before . $post_type->labels->singular_name . $after;
 
        } elseif ( is_attachment() ) {
            $parent = get_post($parent_id);
            $cat = get_the_category($parent->ID); $cat = $cat[0];
            if ($cat) {
                $cats = get_category_parents($cat, TRUE, $delimiter);
                $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
                $cats = str_replace('</a>', '</a>' . $link_after, $cats);
                if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
                echo $cats;
            }
            printf($link, get_permalink($parent), $parent->post_title);
            if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;
 
        } elseif ( is_page() && !$parent_id ) {
            if ($show_current == 1) echo $before . get_the_title() . $after;
 
        } elseif ( is_page() && $parent_id ) {
            if ($parent_id != $frontpage_id) {
                $breadcrumbs = array();
                while ($parent_id) {
                    $page = get_page($parent_id);
                    if ($parent_id != $frontpage_id) {
                        $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
                    }
                    $parent_id = $page->post_parent;
                }
                $breadcrumbs = array_reverse($breadcrumbs);
                for ($i = 0; $i < count($breadcrumbs); $i++) {
                    echo $breadcrumbs[$i];
                    if ($i != count($breadcrumbs)-1) echo $delimiter;
                }
            }
            if ($show_current == 1) {
                if ($show_home_link == 1 || ($parent_id_2 != 0 && $parent_id_2 != $frontpage_id)) echo $delimiter;
                echo $before . get_the_title() . $after;
            }
 
        } elseif ( is_tag() ) {
            echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
 
        } elseif ( is_author() ) {
             global $author;
            $userdata = get_userdata($author);
            echo $before . sprintf($text['author'], $userdata->display_name) . $after;
 
        } elseif ( is_404() ) {
            echo $before . $text['404'] . $after;
        }
 
        if ( get_query_var('paged') ) {
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
            echo __('Page') . ' ' . get_query_var('paged');
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
        }
 
        echo '</div><!-- .breadcrumbs -->';
 
    }
} // end blix_breadcrumbs()


function qt_custom_breadcrumbs() {
    // Settings
    $separator          = '';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumb';
	$currentlang2 = get_bloginfo('language'); 
	if($currentlang2=="pl-PL") {
		$home_title = "Strong główna";
	} 
    if($currentlang2=="en-US") {
		$home_title = 'Home';
	}
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy = 'video';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    
	if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ol id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
           
        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        
		// If post is a custom post type
        $post_type = get_post_type();

        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="active item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="active item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="active item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';

                echo '<li class="active item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
				
                if($post_type == 'video' || $post_type == 'galeria' || $post_type == 'rafalsonik-w-mediach') { 
					$parent_id = wp_get_post_parent_id( $post->ID );
					$parent_id_2 = wp_get_post_parent_id( $parent_id );
					$parent_id_3 = wp_get_post_parent_id( $parent_id_2 );
					
					if(get_permalink( $parent_id_3 )!=get_permalink( $post->ID ) and get_permalink( $parent_id_2 )!=get_permalink( $post->ID )) {
						echo '<li class="item-current item-' . $parent_id_3 . '"><a href="' . get_permalink( $parent_id_3 ) . '">' . get_the_title($parent_id_3 ) . '</a></li>'; 
					}
					
					if(get_permalink( $parent_id_2 )!=get_permalink( $post->ID )) {
						echo '<li class="item-current item-' . $parent_id_2 . '"><a href="' . get_permalink( $parent_id_2 ) . '">' . get_the_title($parent_id_2 ) . '</a></li>'; 
					}
					
					if(get_permalink( $parent_id )!=get_permalink( $post->ID )) {
						echo '<li class="item-current item-' . $parent_id . '"><a href="' . get_permalink( $parent_id ) . '">' . get_the_title($parent_id) . '</a></li>'; 
					}
				}
				
                echo '<li class="active item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="active item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                foreach ( $anc as $ancestor ) {
                    $parents = '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="active item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="active item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="active item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
               
            // Day display
            echo '<li class="active item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
               
            // Month display
            echo '<li class="active item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="active item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="active item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="active item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="active item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ul>';
           
    }
} // end qt_custom_breadcrumbs()



/************************
MENU BOOTSTRAP RESPONSIVE
************************/

class BS3_Walker_Nav_Menu extends Walker_Nav_Menu {

	function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
		$id_field = $this->db_fields['id'];

		if ( isset( $args[0] ) && is_object( $args[0] ) )
		{
			$args[0]->has_children = ! empty( $children_elements[$element->$id_field] );

		}

		return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		if ( is_object($args) && !empty($args->has_children) )
		{
			$link_after = $args->link_after;
			//$args->link_after = ' <b class="caret"></b>';
		}

		parent::start_el($output, $item, $depth, $args, $id);

		if ( is_object($args) && !empty($args->has_children) )
			$args->link_after = $link_after;
	}

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = '';
		$output .= "$indent<ul class=\"dropdown-menu mega-dropdown-menu\">";
	}
}



/************************
Excerpt kropki luk mod
************************/

function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );



/************************
SEO luk mod
************************/

function create_meta_desc() {
    global $post;
	if (is_home() || is_front_page()) {
		echo "<meta name='description' content='YC' />";
	}		
	if (is_single()) {
		$meta = strip_tags($post->post_content);
		$meta_title = strip_tags($post->post_title);
		$meta_size = strlen($meta);
		$meta = strip_shortcodes($meta);
		$meta = str_replace(array("\n", "\r", "\t"), ' ', $meta);
		$meta = substr($meta, 0, 150);
		if($meta_size < 10) {
			echo "<meta name='description' content='$meta_title - YC'/>";
		} else {
			echo "<meta name='description' content='$meta'/>";
		}
	}
}
add_action('wp_head', 'create_meta_desc');


function my_acf_google_map_api( $api ){
	$api['key'] = 'AIzaSyCLDILrB38bdH7b8Tl691gHvV71POUYLfs';
	return $api;
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');




/************************
Przepisy luk mod
************************/

$labels = array(
	'name'               => 'Przepisy',
	'singular_name'      => _x( 'Przepisy', 'post type singular name' ),
	'add_new'            => __( 'Dodaj'),
	'add_new_item'       => __( 'Dodaj przepis' ),
	'edit_item'          => __( 'Edytuj przepis' ),
	'new_item'           => __( 'Nowy przepis' ),
	'all_items'          => __( 'Wszystkie przepisy' ),
	'view_item'          => __( 'Podgląd przepisu' ),
	'search_items'       => __( 'Szukaj przepisu' ),
	'not_found'          => __( 'Nie znaleziono przepisu' ),
	'not_found_in_trash' => __( 'Nie znaleziono przepisu w koszu' ), 
	'parent_item_colon'  => '',
	'menu_name'          => 'Przepisy'
  );
$args = array(
	'labels'        => $labels,
	'description'   => 'Przepisy',
	'public'        => true,
	'menu_position' => 7,
	'supports'      => array( 'title', 'editor', 'thumbnail', 'author'),
	'has_archive' => 'przepisy', 
	'rewrite' => array('slug' => 'przepisy/%przepisy-category%'),

	/* ADD PERMISIONS */
	'capability_type' => 'przepisy',
	'capabilities' => array(
		'publish_posts' => 'publish_przepisy',
		'edit_posts' => 'edit_przepisy',
		'edit_others_posts' => 'edit_others_przepisy',
		'delete_posts' => 'delete_przepisy',
		'delete_others_posts' => 'delete_others_przepisy',
		'read_private_posts' => 'read_private_przepisy',
		'edit_post' => 'edit_przepisy',
		'delete_post' => 'delete_przepisy',
		'read_post' => 'read_przepisy'
		)

);
register_post_type( 'przepisy', $args ); 

// Taxonomy

$labels = array(
	'name'              => _x( 'Kategoria przepisów', 'taxonomy general name' ),
	'singular_name'     => _x( 'Kategoria przepisów', 'taxonomy singular name' ),
	'search_items'      => __( 'Szukaj' ),
	'all_items'         => __( 'Wszystkie kategorie' ),
	'parent_item'       => __( 'Parent' ),
	'parent_item_colon' => __( 'Parent' ),
	'edit_item'         => __( 'Edytuj' ), 
	'update_item'       => __( 'Zaktualizuj' ),
	'add_new_item'      => __( 'Dodaj' ),
	'new_item_name'     => __( 'Nowy' ),
	'menu_name'         => __( 'Kategorie' ),
  );
$args = array(
	'labels' => $labels,
	'hierarchical' => true,
	'show_admin_column' => true,
	'show_ui'           => true,
	'query_var'         => true,
	'rewrite'           => true,
	'rewrite' => array('slug' => 'przepisy'),
  );

register_taxonomy( 'przepisy-category', 'przepisy', $args );


function wpa_course_post_link( $post_link, $id = 0 ){
    $post = get_post($id);  
    if ( is_object( $post ) ){
        $terms = wp_get_object_terms( $post->ID, 'przepisy-category' );
        if( $terms ){
            return str_replace( '%przepisy-category%' , $terms[0]->slug , $post_link );
        }
    }
    return $post_link;  
}
add_filter( 'post_type_link', 'wpa_course_post_link', 1, 3 );

// Taxonomy Przepisy Tags

$args = array(
	'hierarchical' => false,
	'show_admin_column' => true,
	'show_ui'           => true,
	'query_var'         => true,
	'rewrite'           => true,
	'rewrite' => array('slug' => 'tagi'),
);
register_taxonomy('tagi', 'przepisy', $args);




/************************
Produkty luk mod
************************/

$labels = array(
	'name'               => 'Produkty',
	'singular_name'      => _x( 'Produkty', 'post type singular name' ),
	'add_new'            => __( 'Dodaj'),
	'add_new_item'       => __( 'Dodaj produkt' ),
	'edit_item'          => __( 'Edytuj produkt' ),
	'new_item'           => __( 'Nowy produkt' ),
	'all_items'          => __( 'Wszystkie produkty' ),
	'view_item'          => __( 'Podgląd produktu' ),
	'search_items'       => __( 'Szukaj produktu' ),
	'not_found'          => __( 'Nie znaleziono produktu' ),
	'not_found_in_trash' => __( 'Nie znaleziono produktu w koszu' ), 
	'parent_item_colon'  => '',
	'menu_name'          => 'Produkty'
  );
$args = array(
	'labels'        => $labels,
	'description'   => 'Produkty',
	'public'        => true,
	'menu_position' => 7,
	'supports'      => array( 'title', 'editor', 'thumbnail', 'author'),
	'has_archive' => 'produkty', 
	'rewrite' => array('slug' => 'produkty/%produkty-category%'),

	/* ADD PERMISIONS */
	'capability_type' => 'produkty',
	'capabilities' => array(
		'publish_posts' => 'publish_produkty',
		'edit_posts' => 'edit_produkty',
		'edit_others_posts' => 'edit_others_produkty',
		'delete_posts' => 'delete_produkty',
		'delete_others_posts' => 'delete_others_produkty',
		'read_private_posts' => 'read_private_produkty',
		'edit_post' => 'edit_produkty',
		'delete_post' => 'delete_produkty',
		'read_post' => 'read_produkty'
		)

);
register_post_type( 'produkty', $args ); 

// Taxonomy

$labels = array(
	'name'              => _x( 'Kategoria produktów', 'taxonomy general name' ),
	'singular_name'     => _x( 'Kategoria produktów', 'taxonomy singular name' ),
	'search_items'      => __( 'Szukaj' ),
	'all_items'         => __( 'Wszystkie kategorie' ),
	'parent_item'       => __( 'Parent' ),
	'parent_item_colon' => __( 'Parent' ),
	'edit_item'         => __( 'Edytuj' ), 
	'update_item'       => __( 'Zaktualizuj' ),
	'add_new_item'      => __( 'Dodaj' ),
	'new_item_name'     => __( 'Nowy' ),
	'menu_name'         => __( 'Kategorie' ),
  );
$args = array(
	'labels' => $labels,
	'hierarchical' => true,
	'show_admin_column' => true,
	'show_ui'           => true,
	'query_var'         => true,
	'rewrite'           => true,
	'rewrite' => array('slug' => 'produkty'),
  );

register_taxonomy( 'produkty-category', 'produkty', $args );


function wpa_produkty_post_link( $post_link, $id = 0 ){
    $post = get_post($id);  
    if ( is_object( $post ) ){
        $terms = wp_get_object_terms( $post->ID, 'produkty-category' );
        if( $terms ){
            return str_replace( '%produkty-category%' , $terms[0]->slug , $post_link );
        }
    }
    return $post_link;  
}
add_filter( 'post_type_link', 'wpa_produkty_post_link', 1, 3 );



function add_capability() {
	$role = get_role( 'editor' );
	
	/* Przepisy */	
	$role->add_cap('publish_przepisy');
	$role->add_cap('edit_przepisy');
	$role->add_cap('edit_others_przepisy');
	$role->add_cap('delete_przepisy');
	$role->add_cap('delete_others_przepisy');
	$role->add_cap('read_private_przepisy');
	$role->add_cap('edit_przepisy');
	$role->add_cap('delete_przepisy');
	$role->add_cap('read_przepisy');

	/* Produkty */	
	$role->add_cap('publish_produkty');
	$role->add_cap('edit_produkty');
	$role->add_cap('edit_others_produkty');
	$role->add_cap('delete_produkty');
	$role->add_cap('delete_others_produkty');
	$role->add_cap('read_private_produkty');
	$role->add_cap('edit_produkty');
	$role->add_cap('delete_produkty');
	$role->add_cap('read_produkty');
	$role->add_cap('edit_theme_options');	

}  
	
add_action('admin_init', 'add_capability'); 

function remove_menus(){
	if ( ! current_user_can( 'update_core' ) ) {
		//remove_menu_page( 'edit.php' );
		remove_menu_page( 'options-general.php' );
		remove_menu_page( 'edit-comments.php' );
		remove_menu_page( 'upload.php' );
		remove_menu_page( 'tools.php' );
		remove_menu_page( 'profile.php' );
		remove_menu_page( 'index.php' );
	}
}
add_action( 'admin_menu', 'remove_menus' );

add_action( 'init', 'create_post_pub' );

//mod adam support thumbnail
function create_post_pub() {
    add_theme_support( 'post-thumbnails', array( 'post', 'produkty' ) );
    add_theme_support( 'thumbnail', array( 'post', 'produkty' ) );
}
add_filter( 'woocommerce_subcategory_count_html', 'woo_remove_category_products_count' );

function woo_remove_category_products_count() {
    return;
}

function bottega_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Filtry', 'bottega' ),
        'id'            => 'woo-filters',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h4 class="footer-title">',
        'after_title'   => '</h4>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Share', 'bottega' ),
        'id'            => 'share-btns',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h4 class="footer-title">',
        'after_title'   => '</h4>',
    ) );
}
add_action( 'widgets_init', 'bottega_widgets_init' );

function bottega_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'bottega_add_woocommerce_support' );

//// Remove the sorting dropdown from Woocommerce
//remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_catalog_ordering', 30 );
// Remove the result count from WooCommerce
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );
/**
 * Remove related products output
 */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

/**
 * Add custom field to the checkout page
 */
add_action('woocommerce_after_order_notes', 'custom_checkout_field');
function custom_checkout_field($checkout){
    echo '<div id="custom_checkout_field"><h3>' . __('Dodatkowe informacje') . '</h3>';
    woocommerce_form_field( 'client_type_field', array(
        'type'          => 'select',
        'required'      => true,
        'class'         => array( 'client-type-field form-row-wide' ),
        'label'         => __( 'Typ klienta' ),
        'options'       => array(
            'blank'		=> __( 'Wybierz', 'wps' ),
            'company'	=> __( 'Firma', 'wps' ),
            'natural'	=> __( 'Osoba fizyczna', 'wps' )
        )
    ),
        $checkout->get_value( 'client_type_field' ));
    echo '<div id="company_checkout_field"><h3>' . __('Dane firmy') . '</h3>';
    woocommerce_form_field('company_name_field', array(
        'type' => 'text',
        'class' => array(
            'company-name form-row-wide'
        ) ,
        'label' => __('Nazwa firmy') ,
        'placeholder' => __('Nazwa firmy') ,
    ) ,
        $checkout->get_value('company_name_field'));
    woocommerce_form_field('company_street_field', array(
        'type' => 'text',
        'class' => array(
            'company-street form-row-wide'
        ) ,
        'label' => __('Ulica') ,
        'placeholder' => __('Ulica') ,
    ) ,
        $checkout->get_value('company_street_field'));
    woocommerce_form_field('company_city_field', array(
        'type' => 'text',
        'class' => array(
            'company-city form-row-wide'
        ) ,
        'label' => __('Miasto') ,
        'placeholder' => __('Miasto') ,
    ) ,
        $checkout->get_value('company_city_field'));
    woocommerce_form_field('company_code_field', array(
        'type' => 'text',
        'class' => array(
            'company-code form-row-wide'
        ) ,
        'label' => __('Kod pocztowy') ,
        'placeholder' => __('Kod pocztowy') ,
    ) ,
        $checkout->get_value('company_code_field'));
    woocommerce_form_field('company_nip_field', array(
        'type' => 'text',
        'class' => array(
            'company-nip form-row-wide'
        ) ,
        'label' => __('NIP') ,
        'placeholder' => __('NIP') ,
    ) ,
        $checkout->get_value('company_name_field'));
    echo '</div>';
    woocommerce_form_field('invoice_field', array(
        'type' => 'checkbox',
        'class' => array('invoice-field form-row-wide') ,
        'label' => __('Chcę otrzymać fakturę VAT')
    ) ,
        $checkout->get_value('invoice_field'));
    echo '</div>';
}
//* Process the checkout
add_action('woocommerce_checkout_process', 'wps_select_checkout_field_process');
function wps_select_checkout_field_process() {
    global $woocommerce;
    // Check if set, if its not set add an error.
    if ($_POST['client_type_field'] == "blank")
        wc_add_notice( '<strong>Wybierz typ klienta</strong>', 'error' );
}
/**
 * Update the value given in custom field
 */
add_action('woocommerce_checkout_update_order_meta', 'cw_checkout_order_meta');
function cw_checkout_order_meta( $order_id ) {
    if ($_POST['invoice_field']) update_post_meta( $order_id, 'invoice_field', esc_attr($_POST['invoice_field']));
    if ($_POST['client_type_field']) update_post_meta( $order_id, 'client_type_field', esc_attr($_POST['client_type_field']));
    if (!empty($_POST['company_name_field'])) update_post_meta($order_id, 'company_name_field',sanitize_text_field($_POST['company_name_field']));
    if (!empty($_POST['company_street_field'])) update_post_meta($order_id, 'company_street_field',sanitize_text_field($_POST['company_street_field']));
    if (!empty($_POST['company_city_field'])) update_post_meta($order_id, 'company_city_field',sanitize_text_field($_POST['company_city_field']));
    if (!empty($_POST['company_code_field'])) update_post_meta($order_id, 'company_code_field',sanitize_text_field($_POST['company_code_field']));
    if (!empty($_POST['company_nip_field'])) update_post_meta($order_id, 'company_nip_field',sanitize_text_field($_POST['company_nip_field']));
}


add_action( 'init', function(){
    if (  ! is_admin()) {
        if( is_ssl() ){
            $protocol = 'https';
        }else {
            $protocol = 'http';
        }

        /** @var WP_Scripts $wp_scripts */
        global  $wp_scripts;
        /** @var _WP_Dependency $core */
        $core = $wp_scripts->registered[ 'jquery-core' ];
        $core_version = $core->ver;
        $core->src = "$protocol://ajax.googleapis.com/ajax/libs/jquery/$core_version/jquery.min.js";

        if ( WP_DEBUG ) {
            /** @var _WP_Dependency $migrate */
            $migrate         = $wp_scripts->registered[ 'jquery-migrate' ];
            $migrate_version = $migrate->ver;
            $migrate->src    = "$protocol://cdn.jsdelivr.net/jquery.migrate/$migrate_version/jquery-migrate.min.js";
        }else{
            /** @var _WP_Dependency $jquery */
            $jquery = $wp_scripts->registered[ 'jquery' ];
            $jquery->deps = [ 'jquery-core' ];
        }

    }


},11 );

function my_scripts() {
  
  // wp_enqueue_script('jquery');
  wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'));
  wp_enqueue_script('fancybox', get_template_directory_uri() . '/js/jquery.fancybox.min.js', array('jquery'));
  wp_enqueue_script('hammer', get_template_directory_uri() . '/js/hammer.min.js', array('jquery'));
  wp_enqueue_script('owl', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'));
  wp_enqueue_script('main', get_template_directory_uri() . '/js/main.js', array('jquery'));

}
add_action('wp_enqueue_scripts', 'my_scripts', 100);


add_filter( 'woocommerce_get_price_including_tax', 'round_price_product', 1, 1 );
add_filter( 'wc_cart_totals_subtotal_html', 'round_price_product', 1, 1 );
add_filter( 'woocommerce_get_price', 'round_price_product', 1, 1);

function round_up ( $value, $precision ) { 
    $pow = pow ( 10, $precision ); 
    return ( ceil ( $pow * $value ) + ceil ( $pow * $value - ceil ( $pow * $value ) ) ) / $pow; 
} 
function round_price_product( $price ){
    return number_format($price, 2, '.', '');//?:$price);
}

add_filter( 'woocommerce_get_image_size_thumbnail', function( $size ) {
    return [
        'width' => 1500,
        'height' => 150,
        'crop' => 0,
    ];
} );

//Privacy policy checkbox:
add_action( 'woocommerce_review_order_before_submit', 'ephp_add_checkout_privacy_policy', 9 );
function ephp_add_checkout_privacy_policy() {
    woocommerce_form_field( 'privacy_policy', array(
        'type'          => 'checkbox',
        'class'         => array('form-row privacy'),
        'label_class'   => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
        'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
        'required'      => true,
        'label'         => 'Akceptuję <a href="'.get_home_url().'/polityka-prywatnosci">politykę prywatności</a>',
    ));
}
add_action( 'woocommerce_checkout_process', 'ephp_not_approved_privacy' );
function ephp_not_approved_privacy() {
    if ( ! (int) isset( $_POST['privacy_policy'] ) ) {
        wc_add_notice( __( 'Proszę przeczytać i zaakceptować politykę prywatności sklepu aby móc sfinalizować zamówienie.' ), 'error' );
    }
}

//Refund checkbox:
add_action( 'woocommerce_review_order_before_submit', 'ephp_add_checkout_refund', 9 );
function ephp_add_checkout_refund() {
    woocommerce_form_field( 'refund', array(
        'type'          => 'checkbox',
        'class'         => array('form-row refund'),
        'label_class'   => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
        'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
        'required'      => true,
        'label'         => 'Zapoznałem się z prawem do odstąpienia od umowy opisanym w <a href="'.get_home_url().'/regulaminy">regulaminie</a>',
    ));
}
add_action( 'woocommerce_checkout_process', 'ephp_not_approved_refund' );
function ephp_not_approved_refund() {
    if ( ! (int) isset( $_POST['refund'] ) ) {
        wc_add_notice( __( 'Proszę zapoznać się z prawem do odstąpienia od umowy i zaakceptować jego postanowienia' ), 'error' );
    }
}

// Add min value to the quantity field (default = 1)
add_filter('woocommerce_quantity_input_min', 'ephp_min_decimal', 10, 2);
function ephp_min_decimal($val, $product) {
    $weight = get_post_meta($product->id, '_weight', true);
    if($weight){
        return 0.1;
    }
    else{
        return 1;
    }
}

// Add step value to the quantity field (default = 1)
add_filter('woocommerce_quantity_input_step', 'ephp_allow_decimal', 10, 2);
function ephp_allow_decimal($val, $product) {
    $weight = get_post_meta($product->id, '_weight', true);
    if($weight){
        return 0.1;
    }
    else{
        return 1;
    }
}

// Removes the WooCommerce filter, that is validating the quantity to be an int
remove_filter('woocommerce_stock_amount', 'intval');

// Add a filter, that validates the quantity to be a float
add_filter('woocommerce_stock_amount', 'floatval');

// Add unit price fix when showing the unit price on processed orders
add_filter('woocommerce_order_amount_item_total', 'unit_price_fix', 10, 5);
function unit_price_fix($price, $order, $item, $inc_tax = false, $round = true) {
    $qty = (!empty($item['qty']) && $item['qty'] != 0) ? $item['qty'] : 1;
    if($inc_tax) {
        $price = ($item['line_total'] + $item['line_tax']) / $qty;
    } else {
        $price = $item['line_total'] / $qty;
    }
    $price = $round ? round( $price, 2 ) : $price;
    return $price;
}