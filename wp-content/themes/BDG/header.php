<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<?php
	setup_postdata($post);
	$description = get_the_title();
	$description = (wp_strip_all_tags($description));
	if(!empty($description)) {	
		$description = mb_substr($description, 0, 200, 'UTF-8');
	}
	?>
				
    <title><?php bloginfo('name'); ?> - <?php echo @$description; ?></title>
	<!--<link href="https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i" rel="stylesheet">-->
	<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext" rel="stylesheet">
	
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/manifest.json">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">

    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css?<?php echo time(); ?>" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/jquery.fancybox.min.css?<?php echo time(); ?>" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.min.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.min.css?<?php echo time(); ?>" type="text/css" />
	
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/manifest.json">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">


	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window,document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '348876045556009');
	fbq('track', 'PageView');
	</script>

	<noscript>
	<img height="1" width="1" src="https://www.facebook.com/tr?id=348876045556009&ev=PageView&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->
	
	<?php wp_head(); ?>
</head>
  
<body <?php body_class(); ?>>

<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106850339-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-106850339-1');
</script>
<?php global $woocommerce; $cartItems = $woocommerce->cart->get_cart(); ?>
	<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 mini-header-content">
				<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 hidden-xs mini-header-contact">
					<div class="container">
						<ul class="space-between">
							<li>
								<a href="tel:<?php echo str_replace("+","",str_replace(" ","",get_field('numer_telefonu',2))); ?>" target="_blank"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo get_field('numer_telefonu',2); ?></a>
							</li>
							<li>
								<a href="mailto:<?php echo get_field('e-mail',2); ?>" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo get_field('e-mail',2); ?></a>
							</li>
							<li>
								<a href="/newsletter/"><i class="fa fa-bullhorn" aria-hidden="true"></i> Newsletter</a>
							</li>
							<li>
								<a href="https://pl-pl.facebook.com/<?php echo get_field('facebook',2); ?>" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i> <?php echo get_field('facebook',2); ?></a>
							</li>
							<li>
								<?php echo get_field('godziny_otwarcia_-_top',2); ?>
							</li>
							<li class="visible-lg visible-xs">
								<?php echo get_field('adres_-_top',2); ?>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 mini-header-logo">
					<div class="container">
						<a class="logo-wrap" href="/"><img alt="Logo" src="<?php echo get_template_directory_uri(); ?>/img/bottega-del-gusto.svg" class="img-responsive" /></a>
                        <ul class="list-unstyled text-right">
                            <li>
                                <a href="/koszyk"><i class="glyphicon glyphicon-shopping-cart"></i>Koszyk <span class="cart-items"><?php echo count($cartItems); ?></span></a>
                            </li>
                            <li>
                                <a href="/moje-konto"> <?php echo (is_user_logged_in()) ? 'Moje konto': 'Logowanie/Rejestracja'?></a>
                            </li>
                        </ul>
					</div>
				</div>
			</div>
			<div class="container container-fluid">
				<div class="navbar-header">
					<div class="navbar-brand visible-xs">
						MENU
					</div>
					<div class="visible-xs menu-box-position">
						<div class="pull-right">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse">
							<span class="sr-only">MENU</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							</button>
						</div>
					</div>
				</div>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse">
					<div class="menu-header">
						<ul id="menu" class="menu nav navbar-nav">
							<li id="menu-item-165" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-165"><a href="/o-nas/">O nas</a></li>
							<li id="menu-item-165" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-165"><a href="/aktualnosci/">Aktualności</a></li>

							<?php wp_nav_menu(array(
								'container' => '',
								'container_class' => '',
								'items_wrap' => '%3$s',
								'menu' => 'Menu 2',
								'walker' => new BS3_Walker_Nav_Menu
							)); ?>
						</ul>
					</div>
				</div>
			</div>
		</nav>
	</div>
	