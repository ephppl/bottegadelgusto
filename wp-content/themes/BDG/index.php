<?php get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); ?>

<div class="container">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php the_title(); ?>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php the_content(); ?>
	</div>	
</div>

<?php get_footer(); ?>