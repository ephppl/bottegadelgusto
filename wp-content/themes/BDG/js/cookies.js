(function(a) {
	a.divanteCookies = {
		render: function(b) {
			var c = "";
			c += '<div id="cookiesBar"><div class="container"><div class="row table-row-xs"><div class="col-lg-1 col-md-2 col-sm-2 col-xs-12 pull-right" style="text-align:center;"><a id="cookiesBarClose" href="#" title="Zamknij">AKCEPTUJĘ </a></div><div class="col-lg-11 col-md-10 col-sm-10 col-xs-12 pull-left"><div class="text-cookies">Nasz serwis używa plików cookies, aby lepiej spełniać Państwa wymagania. Szczegółowe informacje o plikach cookies można znaleźć w naszej <a href="/polityka-prywatnosci/" title="Polityka prywatności">Polityce Prywatności</a>. <br />Kontynuując przeglądanie serwisu bez zmian ustawień przeglądarki akceptujesz zapisywanie plików cookies.</div></div></div></div></div>', a.cookie("cookie") || (a("body").append(c), a.fn.delegate ? a("#cookiesBar").delegate("#cookiesBarClose", "click", function(b) {
				a.divanteCookies.closeCallback(b)
			}) : a("#cookiesBarClose").bind("click", function(b) {
				a.divanteCookies.closeCallback(b)
			}))
		},
		closeCallback: function(b) {
			return a("#cookiesBar").fadeOut(), a.cookie("cookie") || a.cookie("cookie", !0, {
				path: "/",
				expires: 30
			}), b.preventDefault(), !1
		}
	}
})(jQuery);


(function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)})(function(a){function c(a){return a}function d(a){return decodeURIComponent(a.replace(b," "))}function e(a){0===a.indexOf('"')&&(a=a.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,"\\"));try{return f.json?JSON.parse(a):a}catch(b){}}var b=/\+/g,f=a.cookie=function(b,g,h){if(void 0!==g){if(h=a.extend({},f.defaults,h),"number"==typeof h.expires){var i=h.expires,j=h.expires=new Date;j.setDate(j.getDate()+i)}return g=f.json?JSON.stringify(g):g+"",document.cookie=[f.raw?b:encodeURIComponent(b),"=",f.raw?g:encodeURIComponent(g),h.expires?"; expires="+h.expires.toUTCString():"",h.path?"; path="+h.path:"",h.domain?"; domain="+h.domain:"",h.secure?"; secure":""].join("")}for(var k=f.raw?c:d,l=document.cookie.split("; "),m=b?void 0:{},n=0,o=l.length;o>n;n++){var p=l[n].split("="),q=k(p.shift()),r=k(p.join("="));if(b&&b===q){m=e(r);break}b||(m[q]=e(r))}return m};f.defaults={},a.removeCookie=function(b,c){return void 0!==a.cookie(b)?(a.cookie(b,"",a.extend(c,{expires:-1})),!0):!1}});


jQuery.divanteCookies.render({
	privacyPolicy : true,
	cookiesPageURL : '/polityka-prywatnosci'
});