$(document).ready(function(){
	
	$("[rel='tooltip']").tooltip();    
	$('.box_gallery').hover(
		function(){
			$(this).find('.box_gallery_text').fadeTo(500,1);
		},
		function(){
			$(this).find('.box_gallery_text').fadeTo(500,0);
		}
	); 
	
	if ($('body').hasClass("page-template-kontakt")) {
		if($(window).outerHeight() > 500) {
			$('#map-canvas').height($(window).outerHeight() - 100);
		} else {
			$('#map-canvas').height('600px');
		}
		if($(window).outerWidth() < 768) {
			$('#map-canvas').height('450px');
		}
	}
	
	$(document).scroll(function() {
		if($(document).scrollTop() > 10) {
			$('.mini-header-content').slideUp();
			if($("body").hasClass("home")){
				$('.navbar-default').css('background','rgba(46,46,46,1)').css('border-bottom','1px solid rgba(255,255,255,0)');
			} else {
				$('.navbar-default').css('background','rgba(46,46,46,1)');			
			}
		} else {
			$('.mini-header-content').slideDown();
			$('.navbar-default').css('background','').css('border-bottom','');				
		}
	});
});

$(document).ready(function(){
    $(".menu-item-has-children").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            $(this).toggleClass('open');       
        }
    );
});

// luk mod - active przepisy
$(document).ready(function($) {
	if($(".przepisy-active").length != 0){
		var adr = window.location.href;
		var arr = adr.split('/');
		$('.menu-'+arr[4]).addClass("active");
		$('.menu-'+arr[4]+'-'+arr[5]).addClass("active");
	}
});
$(document).ready(function() {
    var elems = $('#company_checkout_field');
    elems.hide();
    $('#client_type_field').on('change', function () {
    	console.log('Wartość: '+$(this).val());
        if($(this).val() !== ''){
            if($(this).val() === 'company'){
                elems.show();
            }
            else{
				elems.hide();
            }
        }
    });
    jQuery.fn.ForceNumericOnly =
        function() {
    	this.keyup(function(e) {
                $(this).val($(this).val().replace(/,/g,"."));
            });
        };
    $(".quantity .qty").ForceNumericOnly();
});