<?php 
/**
 * Template name: Wszystkie produkty
 */
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language');
the_post();
if(is_shop() or is_product_category() or is_product_taxonomy() or is_product()): ?>
    <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('<?php echo get_field('top_banner'); ?>');">
        <div class="caption-over-block-all">
            <div class="caption-over-outer-all">
                <div class="caption-over-inner-all top-banner-padding">
                    <div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
                        <h1><?php the_title(); ?></h1>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down-white.png" class="img-responsive top-banner-twig">
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
    <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 products-recent products-home" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.jpg');">
        <div class="container page-default recomendation-page">
            <img src="<?php echo get_template_directory_uri(); ?>/img/twig-promo-down.png" class="img-responsive twig-promo-down-products" />
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="font-family: 'Open Sans', sans-serif;">
                <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 taxonomy-right pull-right">
                    <?php echo do_shortcode("[products limit='12' columns='3' orderby='date' order='DESC' paginate='true']"); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>