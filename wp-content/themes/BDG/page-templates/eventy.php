<?php 
/**
 * Template name: Eventy
 */
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); ?>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('<?php echo get_field('top_banner'); ?>');">
	<div class="caption-over-block-all">
		<div class="caption-over-outer-all">
			<div class="caption-over-inner-all top-banner-padding">
				<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
					<h1><?php echo get_the_title(); ?></h1>
					<img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down-white.png" class="img-responsive top-banner-twig">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 recipes-home-coffee" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.jpg');">
	<div class="container">
		<div class="col-lr-0 col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12" >
			<div class="recipes-home-img-caffee hidden-xs" style="background-image:url('<?php echo get_field('eventy-grafika') ?>"></div>
				
			<div class="col-lr-0 col-lg-6 col-lg-offset-6 col-md-6 col-md-offset-6 col-sm-8 col-sm-offset-4 col-xs-12 recipes-home-content" style="border: 10px solid #e9e9e9;">	
				<?php echo get_field('eventy-text'); ?>
			</div>	
		</div>
	</div>
</div>

<?php 
	$images = get_field('zdjecia', $child_id);
	if(!empty($images)){
?>
	<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 gallery">
		<div class="container">
			<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 center">
				<h2 style="color;#000;">Galeria</h2>
				<img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down.png" class="img-responsive twig-slider-down">
			</div>
			<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 gallery-box">
			<?php 
				if( $images ): 
					$i = 1;
					foreach( $images as $image ): 
						?>
							<a href="<?php echo $image['sizes']['large']; ?>" data-fancybox="images">
								<div class="col-lr-0 col-lg-3 col-md-4 col-sm-6 col-xs-12 h-lg-3 h-md-4 h-sm-6 h-xs-12">
									<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 category-box">
										<div id="parent-553" class="parent-page center h-content" style="background-image: url('<?php echo $image['sizes']['medium']; ?>'); background-size: cover; background-repeat: no-repeat; background-position: center center;">
											<div class="caption-over-block-all">
												<div class="caption-over-outer-all">
													<div class="caption-over-inner-all">
														<h3>Zobacz</h3>
														<span class="twig">&nbsp;</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php 
						if($i / 4 == 0){ ?>
							<div class="row">
							</div>
						<?php }
						$i++;
					endforeach;
				 endif; 
			?>
			</div>
		</div>
	</div>
<?php }	
?>
<?php get_footer(); ?>