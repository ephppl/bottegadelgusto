<?php 
/**
* Template name: Kontakt
**/
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); 
?>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyCLDILrB38bdH7b8Tl691gHvV71POUYLfs"></script>
	<script type="text/javascript">
	var markers = [
		<?php 
		$lat_lng = get_field('lokalizacja');
		if($lat_lng['lat'] != "") :
			?>
			{
				"title": '',
				"lat": '<?php echo $lat_lng['lat']; ?>',
				"lng": '<?php echo $lat_lng['lng']; ?>',
				"description": '<?php echo str_replace("<p>", '',str_replace("</p>", '',str_replace("\n", '', get_field('opis_lokalizacji')))); ?>',
			},
			<?php 
		endif;
		?>
	];
	window.onload = function () {
		LoadMap();
	}
	function LoadMap() {
		var mapOptions = {
			center: new google.maps.LatLng(<?php echo $lat_lng['lat']+0.002; ?>,<?php echo $lat_lng['lng']; ?>),
			zoom: 15,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			scrollwheel: false,
			styles: [{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"stylers":[{"hue":"#00aaff"},{"saturation":-100},{"gamma":2.15},{"lightness":12}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"lightness":24}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":57}]}]
		};
		var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
 
		var infoWindow = new google.maps.InfoWindow();
		
		var image = '/wp-content/themes/BDG/img/pointer.png';
		
		for (var i = 0; i < markers.length; i++) {
			var data = markers[i];
			var myLatlng = new google.maps.LatLng(data.lat, data.lng);
			var marker = new google.maps.Marker({
				position: myLatlng,
				map: map,
				icon: image,
				title: data.title
			});
 
			(function (marker, data) {
				google.maps.event.addListener(marker, "click", function (e) {
					infoWindow.setContent("<div>" + data.description + "</div>");
					infoWindow.open(map, marker);
				});
				google.maps.event.addListener(marker, 'dblclick', function(e) {
					map.setCenter(marker.getPosition());
					map.setZoom(15);
				});
			})(marker, data);
		}
	}
	</script>
	<div id="map-canvas" style="height:400px"></div>
</div>
	
<?php get_footer(); ?>