<?php 
/**
 * Template name: Newsletter
 */
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); ?>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('<?php echo get_field('top_banner', 580); ?>');">
	<div class="caption-over-block-all">
		<div class="caption-over-outer-all">
			<div class="caption-over-inner-all top-banner-padding">
				<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
					<h1><?php echo get_the_title(); ?></h1>
					<img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down-white.png" class="img-responsive top-banner-twig">
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 recipes-home" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.jpg');">
	<div class="container">
		<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 background-white">
			<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-big">
				<h2>Zapisz się już teraz!</h2>
				
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>