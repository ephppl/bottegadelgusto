<?php 
/**
 * Template name: o nas
 */
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); ?>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('<?php echo get_field('top_banner'); ?>');">
	<div class="caption-over-block-all">
		<div class="caption-over-outer-all">
			<div class="caption-over-inner-all top-banner-padding">
				<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
					<h1><?php echo get_the_title(); ?></h1>
					<img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down-white.png" class="img-responsive top-banner-twig">
				</div>
			</div>
		</div>
	</div>
</div>
<!--
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 products-home about-us-home" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.svg');">
	<div class="container">
		<img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down.svg" class="img-responsive twig-slider-down">
		<div class="col-lr-0 col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 white-background">
			<?php echo get_field('opis',11); ?>
		</div>
	</div>
</div>
-->
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 products-home about-us-home">
	<div class="container">
		<img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down.png" class="img-responsive twig-slider-down">
		<div class="col-lr-0 col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 background-white">
			<?php echo get_field('opis',11); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>