<?php 
/**
 * Template name: Strona główna
 */
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); ?>

<header id="sliders" class="carousel slide col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<ol id="punktor" class="carousel-indicators hidden-xs">
		<?php if( have_rows('slider') ): ?>
			<?php $x=0;
				while( have_rows('slider') ): the_row(); 
				$zdjecie = get_sub_field('banner');
				$datau = get_sub_field('data_usuniecia');
				$datau = str_replace('/', '-', $datau);

			$datad = get_sub_field('data_dodania');
				$datad = str_replace('/', '-', $datad);
				$datad = strtotime($datad);
				if(time() >= $datad) { 
					if($datau != "") {
						$datau = strtotime($datau);
						if(time() <= $datau) { 
						if($x==0) { $active = ' class="active"'; } else { $active = ''; }
						echo '<li data-target="#sliders" data-slide-to="'. $x.'"'.$active.'></li>'; $x++; }
					} else { 
						if($x==0) { $active = ' class="active"'; } else { $active = ''; }
						echo '<li data-target="#sliders" data-slide-to="'. $x.'"'.$active.'></li>'; $x++; 
					}
				} else {
					$visible = 0;
				}
			?>
			<?php endwhile; ?>
		<?php endif; ?>
	</ol>

	<div id="slider" class="carousel-inner">
		<?php if( have_rows('slider') ): ?>
			<?php $x=0; while( have_rows('slider') ): the_row(); 
					$visible = 0;
					$zdjecie = get_sub_field('banner');
					$target = get_sub_field('target');
					$text = get_sub_field('tytul');
					$link = str_replace("https://","",str_replace("http://","",get_sub_field('odnosnik')));
					
					$datau = get_sub_field('data_usuniecia');
					$datau = str_replace('/', '-', $datau);
				
					$datad = get_sub_field('data_dodania');
					$datad = str_replace('/', '-', $datad);
					$datad = strtotime($datad);	
					
					if(time() >= $datad) { 
						if($datau != "") {
							$datau = strtotime($datau);
							if(time() >= $datau) { $visible = 0; } else { $visible = 1; }
						} else { $visible = 1; }
					}
				if($visible == 1) {
				?>
				<div class="item<?php if($x==0) { echo " active"; } ?>">
					<?php if(!empty($link)) { ?>
						<a href="http://<?php echo $link; ?>" target="<?php echo $target; ?>">
					<?php } ?>
						<div class="fill" style="background-image:url('<?php echo $zdjecie; ?>');"></div>
						<?php if(!empty($text)) { ?>
							<div class="carousel-caption">
								<div class="slider-box">
									<!--<div class="standard-box-text-line"></div>-->
									<?php echo $text; ?>
								</div>
							</div>
						<?php } ?>
					<?php if(!empty($link)) { ?>
						</a>
					<?php } ?>
				</div>
			<?php $x++; } endwhile; ?>
		<?php endif; ?>
		<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 trail" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/slider_trail.png');"></div>
	</div>
	<!--
	<a class="left carousel-control hidden-xs" href="#sliders" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
	<a class="right carousel-control hidden-xs" href="#sliders" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
	-->
	<a class="left carousel-control hidden-xs" href="#sliders" data-slide="prev"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-prev.svg" class="glyphicon-chevron-left"></a>
	<a class="right carousel-control hidden-xs" href="#sliders" data-slide="next"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-next.svg" class="glyphicon-chevron-right"></a>
</header>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 about-us-home">
	<div class="container">
		<img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down.png" class="img-responsive twig-slider-down">
		<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
			<?php echo get_field('opis_-_strona_glowna',11); ?>
		</div>
	</div>
</div>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 about-search">
	<div class="container">
		<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 about-search-title">
			<h1>Co znajdziesz w naszym sklepie?</h1>
		</div>
		<img src="<?php echo get_template_directory_uri(); ?>/img/twig-title-black.svg" class="img-responsive twig-title-black">
	</div>
</div>


<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 products-home" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.jpg');">
	<div class="container">
	<?php $terms = get_terms('product_cat',array('hide_empty' => true, 'parent' => 0)); ?>
	<?php foreach ( $terms as $term ) { 

		$img = get_field('miniaturka_na_stronie_glownej', $term);
		if(!$img) {
		    $thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true );
		    $img = wp_get_attachment_url( $thumbnail_id );
		} 
		?>
		<a href="/product-category/<?php echo $term->slug; ?>" title="<?php echo $term->name; ?>">
			<div class="col-lr-0 col-lg-3 col-md-4 col-sm-6 col-xs-12 h-lg-3 h-md-4 h-sm-6 h-xs-12">
				<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 category-box" style="background-image: url('<?php echo $img; ?>');">
					<div id="parent-<?php the_ID(); ?>" class="parent-page center h-content">
						<div class="caption-over-block-all">
							<div class="caption-over-outer-all">
								<div class="caption-over-inner-all">
									<h1><?php echo $term->name; ?></h1>
									<img src="<?php echo get_template_directory_uri(); ?>/img/twig-products.png" class="img-responsive twig-products">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</a>
	<?php 
	}
	wp_reset_query(); 
	?>
	</div>
</div>



<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 promo-home">
	<div class="container">
		<!--<img src="<?php echo get_template_directory_uri(); ?>/img/promo-img.png" class="img-responsive" style="width:900px; height:auto; margin:0px auto;">-->
		<img src="<?php echo get_template_directory_uri(); ?>/img/twig-promo-down.svg" class="img-responsive twig-promo-down">
	</div>
</div>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 about-search">
    <div class="container">
        <div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 about-search-title">
            <h1>Ostatnio dodane produkty</h1>
        </div>
        <img src="<?php echo get_template_directory_uri(); ?>/img/twig-title-black.svg" class="img-responsive twig-title-black">
    </div>
</div>
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 products-recent">
    <div class="container">
        <?php echo do_shortcode("[recent_products limit='12' columns='3' orderby='date' order='DESC']"); ?>
    </div>
</div>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 recipes-home" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/recipes-bg-home.jpg');">
	<div class="container">
		<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 recipes-home-title">
			Gotowe pomysły na włoskie danie
		</div>
		<img src="<?php echo get_template_directory_uri(); ?>/img/twig-title-white.svg" class="img-responsive twig-title-white">
	</div>
	
	<div class="container">
	<?php
	$args = array(
		'post_type'      => 'przepisy',
		'posts_per_page' => 1
	);

	$parent = new WP_Query($args);

	if($parent->have_posts()) {
		while($parent->have_posts()) { $parent->the_post(); 
	?>
		<div class="col-lr-0 col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12">
			<div class="recipes-home-img" style="background-image:url('<?php $zdjecie_przepisu = get_field('zdjecie_przepisu'); echo $zdjecie_przepisu['url']; ?>');"></div>
				
			<div class="col-lr-0 col-lg-4 col-lg-offset-8 col-md-4 col-md-offset-8 col-sm-5 col-sm-offset-7 col-xs-12 recipes-home-content">	
				<strong><?php the_title(); ?></strong>
				
				<br /><br />
				
				<strong>Rodzaj:</strong> <?php $przepisy_kategorie = get_the_terms(get_the_ID(),'przepisy-category'); echo $przepisy_kategorie[0]->name; ?>
				
				<br />
				
				<strong>Czas przygotowania:</strong> <?php echo get_field('czas_przygotowania'); ?>
				
				<br />
				
				<strong>Potrzebne produkty:</strong> 
				<?php 
				echo fredy_custom_excerpt(get_field('skladniki'));
				//echo the_field('skladniki'); ?> <br />
				<!--<ul>
				<?php 
				if( have_rows("potrzebne_produkty") ):
					while( have_rows("potrzebne_produkty") ): the_row();
						echo '<li>';
						echo get_sub_field('nazwa_produktu');
						echo ' ';
						echo get_sub_field('ilosc_produktu');
						echo '</li>';
					endwhile; 
				endif; 
				?>
				</ul>-->
				
				<br />
					
				<a href="/przepisy/<?php // the_permalink(); ?>" title="<?php the_title(); ?>">
					<button type="button" class="btn btn-default">Więcej</button>
				</a>
			</div>	
		<?php 
			}
		}
		wp_reset_query(); 
		?>
		</div>
	</div>
</div>

<script>
	$(document).ready(function($) {  
		$('#sliders').hammer().on('swipeleft', function(){
			$(this).carousel('next'); 
		});
		$('#sliders').hammer().on('swiperight', function(){
			$(this).carousel('prev'); 
		});

		$('a').attr('draggable', 'false');
		$('img').on('dragstart', function(event) { event.preventDefault(); }); 
	}); 
	$('.carousel').carousel({
        interval: 5000
    });
</script>


</div>

<?php get_footer(); ?>