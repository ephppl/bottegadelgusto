<?php 
/**
 * Template name: test-produkty
 */
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); ?>
<style>
.taxonomy-left{
	color:#000!important;
	background-color: #fff;
	padding:30px;
}
.taxonomy-left a,
.taxonomy-left a:active{
	color:#000!important;
}
.taxonomy-left a:hover,
.taxonomy-left a:focus{
	color: #FED401;
}

.product-box {
	background: #fff;
}

.product-link {
	display:block;
	color:#000;
}

.product-image {
	margin: 0 auto;
}

.product-title {
	text-align:center;
	min-height: 50px;
}

.product-price {
	text-align:center;
	font-weight:bold;
	font-size:25px;
}

.product-category li.active {
	font-weight:bold;
}
</style>
<?php /*Pobiera aktualny taxonomy*/ $tax = $wp_query->get_queried_object(); ?>

<?php 
	if($tax->slug == 'wina' && !isset($_GET['a'])) {
		header('Location: http://bottegadelgusto.pl/wina');
		exit();
	}
	else {
?>

<!--<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('<?php echo get_field('top_banner', 'term_'.$tax->term_id); ?>');">
	<div class="caption-over-block-all">
		<div class="caption-over-outer-all">
			<div class="caption-over-inner-all top-banner-padding">
				<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
					<h1>Kategoria: <?php echo single_cat_title("", false); ?></h1>
					<img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down-white.svg" class="img-responsive top-banner-twig">
				</div>
			</div>
		</div>
	</div>
</div>-->
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 products-home" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.jpg');">
	<img src="<?php echo get_template_directory_uri(); ?>/img/twig-promo-down.png" class="img-responsive twig-promo-down-products">
	<div class="col-lr-0 container">
		<!--<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 center">
			<?php echo get_field('tekst_wprowadzajacy'); ?>
		</div>-->
		
		<div class="col-lr-0 col-lg-3 col-md-3 col-sm-12 col-xs-12 taxonomy-left">
			<style>
				#searchbox input.searchform {
					border: 1px solid #000;
					float:left;
					width: 70%;
					padding:4px;
					height:35px;
					font-size:14px;
				}
				#searchbox input.submit {
					float:left;
					border: 1px solid #000;
					padding:4px;
					margin-left: 4px;
					background:#2e2e2e;
					color:#fff;
					font-size:14px;
					height:35px;
					width:60px;
				}
			</style>
			<div id="searchbox">
				Wyszukaj <br />
				<form id="searchform" class="form-inline" role="search" method="get" action="<?php bloginfo('url'); ?>/">
					<div class="form-group searchform">
						<input type="search" class="searchform" placeholder="wpisz frazę..." value="<?php the_search_query(); ?>" name="s" id="s" />
						<input type="hidden" name="post_type" value="produkty" />
						<input type="submit" class="submit" name="submit" id="searchsubmit" value="szukaj" />
					</div>
				</form>
			</div>
			
			<br />	
		
			<a href="/produkty/">Kategorie produktów</a>
			<ul class="product-category">
			<?php $terms = get_terms(array('taxonomy' => 'produkty-category','orderby' => 'menu_order','order' => 'desc','hide_empty' => false)); ?>
			<?php foreach ( $terms as $term ) { ?>
				<?php $this_category = get_the_terms(get_the_ID(),'produkty-category'); ?>
				<li <?php if($_GET['cat'] == $term->slug)   { echo 'class="active"'; } ?>>
					<a href="/testowe-produkty/?cat=<?php echo $term->slug; ?>" class="category_url">
					<?php echo $term->name; ?>
				</a></li>
			<?php } ?>
			</ul>
		</div>
		<div class="col-lr-0 col-lg-9 col-md-9 col-sm-12 col-xs-12 taxonomy-right">
			<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
			<?php 
			
				if(!isset($_GET['cat'])) {
					header("Location: /testowe-produkty/?cat=oliwy");
					die();
				}				
			
				if(isset($_GET['cat'])) {			
					$args = array(
						'posts_per_page' => -1,
						'post_type' => 'produkty',
						//'orderby' => 'menu_order',
						//'order' => 'desc',
						'tax_query' => array(
							array(
								'taxonomy' => 'produkty-category',
								'field'    => 'slug',
								'terms'    => $_GET['cat'],
							),
						),
					);
				}
				@$myquery = new WP_Query( $args );				
					$i = 1; 
					while ($myquery->have_posts()) {							
						$myquery->the_post();
						$all_meta = get_post_meta(get_the_ID());
						@$galeria = get_field('galeria');
						$cena = get_field('cena');
					
					//Tutaj tworzenie informacji o produkcie w kategorii 
					// h-lg-3 h-md-4 h-sm-6 h-xs-12 ?>
					
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-box category-box-padding">
						<a class="product-link" href="<?php echo get_permalink();?>">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-image"><img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'medium'); ?>" class="img-responsive"></div>
						</a>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-title"><?php echo get_the_title(); ?></div>
							<?php if(get_field('cena', $post->ID)) { ?>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-price"><?php echo $cena; ?></div>
							<?php } ?>
						</div>
					</div>
					<?php 
						//echo $i;
						
						
						
						if(($i%3) == 0) { ?>
						<div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;<br /></div></div>
					<?php } 
					$i++; 
					}				
				?>
				</div>
					
			
			<!-- mod adam
			<div class="row row-centered" id="gallery">
				<?php $galeria = get_field('galeria'); ?>
				<?php $count = count(get_field('galeria')); ?>
				<?php $modulo = $count%4; ?>
				<?php $i=0; ?>
				<?php if(!empty($galeria)) { ?>
					<?php foreach ( $galeria as $zdjecia ) { ?>
						<a data-fancybox="group" href="<?php echo $zdjecia['url']; ?>" title="galeria">
							<div class="col-lr-0 col-lg-3 col-md-4 col-sm-6 col-xs-12 h-lg-3 h-md-4 h-sm-6 h-xs-12 category-box-padding<?php if($i >= ($count-$modulo)) { echo ' col-centered'; } ?>">
								<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 category-box" style="background-image: url('<?php echo $zdjecia['url']; ?>');">
									<div id="parent-<?php the_ID(); ?>" class="parent-page center h-content">
										<div class="caption-over-block-all">
											<div class="caption-over-outer-all">
												<div class="caption-over-inner-all">
													<h1>Powiększ</h1>
													<span data-fancybox="test" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/twig-products.png');" class="twig-products">
													</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</a>
					<?php 
					$i++;
					}
				}
				wp_reset_query(); 
				?>
			</div>-->
		</div>
	</div>
</div>

<?php } ?>

<?php get_footer(); ?>