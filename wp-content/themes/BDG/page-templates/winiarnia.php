<?php 
/**
 * Template name: Winiarnia
 */
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); ?>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('<?php echo get_field('top_banner'); ?>');">
	<div class="caption-over-block-all">
		<div class="caption-over-outer-all">
			<div class="caption-over-inner-all top-banner-padding">
				<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
					<h1><?php echo get_the_title(); ?></h1>
					<img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down-white.png" class="img-responsive top-banner-twig">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 recipes-home-coffee" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.jpg');">
	<div class="container">
		<div class="col-lr-0 col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12" >
			<div class="recipes-home-img-caffee hidden-xs" style="background-image:url('<?php echo get_field('o_kawiarni_-_grafika') ?>"></div>
				
			<div class="col-lr-0 col-lg-6 col-lg-offset-6 col-md-6 col-md-offset-6 col-sm-8 col-sm-offset-4 col-xs-12 recipes-home-content" style="border: 10px solid #e9e9e9;">	
				<?php echo get_field('o_kawiarni'); ?>
			</div>	
		</div>
	</div>
</div>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 promo-home">
	<div class="container">
		<img src="<?php echo get_field('grafika_reklamowa'); ?>" class="img-responsive" style="width:900px; height:auto; margin:0px auto;">
		<img src="<?php echo get_template_directory_uri(); ?>/img/twig-promo-down.svg" class="img-responsive twig-promo-down">
	</div>
</div>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 recipes-home-coffee" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.jpg');">
</div>
<?php get_footer(); ?>