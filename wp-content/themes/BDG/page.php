<?php 
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language');
the_post();?>
<?php if(is_shop() or is_product_category() or is_product_taxonomy() or is_product()): ?>
    <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('<?php echo get_field('top_banner'); ?>');">
        <div class="caption-over-block-all">
            <div class="caption-over-outer-all">
                <div class="caption-over-inner-all top-banner-padding">
                    <div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
                        <h1><?php the_title(); ?></h1>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down-white.png" class="img-responsive top-banner-twig">
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if(is_shop() or is_product_category() or is_product_taxonomy() or is_product() or is_cart() or is_checkout() or is_account_page()): ?>
    <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 products-home" style="background-image:url('http://bottegadelgusto.pl/wp-content/themes/BDG/img/products-bg-home.jpg');">
<?php endif; ?>
<?php if(is_product_category() or is_product_taxonomy() or is_product() or is_cart() or is_checkout() or is_account_page()): ?>
    <img src="<?php echo get_template_directory_uri(); ?>/img/twig-promo-down.png" class="img-responsive twig-promo-down-products" />
<?php endif; ?>
<div class="container page-default recomendation-page">
    <?php if(!is_shop() or is_product_category() or is_product_taxonomy() or is_product() or is_account_page()): ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1><?php the_title(); ?></h1>
        </div>
    <?php endif; ?>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="font-family: 'Open Sans', sans-serif;">
        <?php if(is_product_category() or is_product_taxonomy() or is_product()): ?>
            <div class="col-lr-0 col-lg-9 col-md-9 col-sm-12 col-xs-12 taxonomy-right pull-right">
                <?php the_content(); ?>
            </div>
            <div class="col-lr-0 col-lg-3 col-md-3 col-sm-12 col-xs-12 taxonomy-left pull-left">
                <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 taxonomy-left__color">
                    <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <div id="searchbox">
                            <span class="search-title">Wyszukaj</span>
                            <?php get_product_search_form(); ?>
                        </div>
                    </div>
                    <?php if(is_product_category() or is_product_taxonomy()): ?>
                        <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <?php if ( ! dynamic_sidebar( 'woo-filters' ) ) : ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <div class="col-lr-0 col-lg-12 col-md-12 hidden-sm hidden-xs ">
                        <a href="/e-sklep">Kategorie produktów</a>
                        <ul class="product-category">
                            <?php $tax = get_queried_object(); $terms = get_terms(array('taxonomy' => 'product_cat', 'parent' => 0, 'orderby' => 'menu_order','order' => 'desc','hide_empty' => false)); ?>
                            <?php foreach ( $terms as $term ): ?>
                                <?php $this_category = get_the_terms(get_the_ID(),'product_cat'); ?>
                                <?php if($term->slug != 'uncategorized'): ?>
                                <li <?php if($tax->slug == $term->slug)   { echo 'class="active"'; } ?>>
                                    <a href="/product-category/<?php echo $term->slug; ?>" class="category_url">
                                        <?php echo $term->name; ?>
                                    </a>
                                    <ul class="children">
                                        <?php
                                        $children = get_term_children($term->term_id, 'product_cat');
                                        if(!empty($children)):
                                            $terms_child = get_terms(array('taxonomy' => 'product_cat', 'parent' => $term->term_id, 'orderby' => 'menu_order','order' => 'desc','hide_empty' => false));
                                        ?>
                                            <?php foreach ( $terms_child as $termc ): ?>
                                                <li <?php if($tax->slug == $termc->slug): echo 'class="active"'; endif; ?>>
                                                    <a href="/product-category/<?php echo $termc->slug; ?>" class="category_url">
                                                        <?php echo $termc->name; ?>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="col-lr-0 hidden-lg hidden-md col-sm-12 col-xs-12">
                        <div class="col-lr-0 category-title">
                            <a href="/e-sklep/">Kategorie produktów</a>
                        </div>
                        <div class="col-lr-0 nav-container">
                            <div class="form-group">
                                <select class="form-control nav" onchange="location = this.value;">
                                    <?php $tax = $wp_query->get_queried_object(); $terms = get_terms(array('taxonomy' => 'product_cat','orderby' => 'menu_order','order' => 'desc','hide_empty' => false)); ?>
                                    <?php foreach ( $terms as $term ): ?>
                                        <?php $this_category = get_the_terms(get_the_ID(),'product_cat'); ?>
                                        <?php if($term->slug != 'uncategorized'): ?>
                                            <option <?php if($tax->slug == $term->slug)   { echo 'class="active"'; } ?> value="/product-category/<?php echo $term->slug; ?>" class="category_url">
                                                <?php echo $term->name; ?>
                                            </option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <?php the_content(); ?>
        <?php endif; ?>
	</div>
</div>
<?php if(is_shop() or is_product_category() or is_product_taxonomy() or is_product() or is_cart() or is_checkout() or is_account_page()): ?></div><?php endif; ?>
<?php get_footer(); ?>