<?php 
function string_limit_words($string, $word_limit){
	$words = explode(' ', $string, ($word_limit + 1));
	if(count($words) > $word_limit)
	array_pop($words);
	return implode(' ', $words);
}
?>
<div id="sticky">	
	<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-active">
		<h4>Kategorie</h4>
		<?php $terms = get_terms('przepisy-category'); ?>
		<?php foreach ( $terms as $term ) { ?>
			<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 menu-<?php echo $term->slug; ?>">
				<a href="/przepisy/<?php echo $term->slug; ?>">» <?php echo $term->name; ?> ( <?php echo $term->count; ?> )</a>
			</div>
		<?php } ?>
		<?php wp_reset_query(); ?>
	</div>




	<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-active">
		<h4>Popularne</h4>
		<?php
			@$myquery = new WP_Query(array(
				'post_type' => 'przepisy',
				'posts_per_page' => 3,
				'date_query'    => array(
					'column'  => 'post_date',
					'after'   => '- 240 days'
				),
				'meta_key'			=> 'wyswietlenia_wpisu',
				'orderby'			=> 'meta_value',
				'order'				=> 'DESC'
			));
			while ($myquery->have_posts()) {
				$myquery->the_post();
			?>
			<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 menu-<?php echo $post->post_name; ?>">
				<a href="<?php the_permalink(); ?>">» <?php echo string_limit_words(get_the_title(),4); ?></a>
			</div>
		<?php } ?>
		<?php wp_reset_query(); ?>
	</div>


	<!--
	<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-active">
		<br />
		<h4><?php echo __('Tags','Befrendi'); ?></h4>
		<?php 
		$args = array('number' => '15'); 
		$terms = get_terms(__('tags','Befrendi-przepisy'),$args); 
		$i = 1;
		foreach ( $terms as $term ) { ?>
			<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 menu-<?php echo $term->slug; ?>">
				<a href="/<?php echo __('tags','Befrendi-przepisy'); ?>/<?php echo $term->slug; ?>"><?php echo $term->name; ?></a><?php if(count($terms)>$i) { echo ','; } ?>
			</div>
			<?php $i++; ?>
		<?php } ?>
		<?php wp_reset_query(); ?>
	</div>
	-->
</div>

<script>
	$(document).ready(function(){
		if($(window).width() > 992) {
			$("#sticky").sticky({topSpacing:120,zIndex:1000});
		}
	});
</script>