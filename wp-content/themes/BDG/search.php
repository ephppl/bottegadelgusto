<?php 
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); ?>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('http://bottegadelgusto.pl/wp-content/themes/BDG/img/produkty.jpg');">
	<div class="caption-over-block-all">
		<div class="caption-over-outer-all">
			<div class="caption-over-inner-all top-banner-padding">
				<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
					<h1>Wyniki wyszukiwania</h1>
					<img src="http://bottegadelgusto.pl/wp-content/themes/BDG/img/twig-slider-down-white.png" class="img-responsive top-banner-twig">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 products-home" style="background-image:url('http://bottegadelgusto.pl/wp-content/themes/BDG/img/products-bg-home.jpg');">
	<img src="<?php echo get_template_directory_uri(); ?>/img/twig-promo-down.png" class="img-responsive twig-promo-down-products">
	<div class="col-lr-0 container">	
		<div class="col-lr-0 col-lg-9 col-md-9 col-sm-12 col-xs-12 taxonomy-right pull-right">
			<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
				<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 search-results_background">
				   	Wyniki wyszukiwania dla: <strong><?php echo get_search_query(); ?></strong>
				</div>
				<?php  if (have_posts()): ?>
					<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">	
						<?php 
						$i = 1; 
						global $wp_query;
	                    $count = $wp_query->found_posts;; 
	                    $liczbaPostow = $count;
						while(have_posts()): the_post(); ?>
							
							 <?php if(($i%3) == 1 ) { ?>
	                            <div class="row row-eq">
	                        <?php } ?>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-eq">
		                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-box category-box-padding">
			                            <a class="product-link" href="<?php echo get_permalink();?>">
			                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-image">
			                                   <?php if ( has_post_thumbnail() ) { ?>
													<div style="text-align: center;" class="search-thumb">
														<img style="margin: 0 auto;" src="<?php the_post_thumbnail_url('medium'); ?>" class="search-thumb img-responsive" >
														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-title"><?php echo get_the_title(); ?></div>
													</div>
												<?php } else { ?>
													<div style="text-align: center;" class="search-thumb">
														<img style="margin: 0 auto;" src="<?php echo get_template_directory_uri(); ?>/img/brak_zdjecia.jpg" class="search-thumb img-responsive">
														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-title"><?php echo get_the_title(); ?></div>
													</div>
												<?php } ?>
			                                </div>
			                            </a>
		                                <?php // if(get_field('cena', $post->ID)) { ?>
		                                     <!--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-price"><?php echo $cena; ?></div>-->
		                                <?php // } ?>
		                            </div>
		                        </div>
	                        <?php if(($i%3) == 0 && $i != 0) { ?>
	                            </div> 
	                        <?php } elseif ($i == $liczbaPostow && $i != 0) { ?>
	                            </div>
	                        <?php } ?>		
							
							<?php $i++; ?>
						
						<?php endwhile; ?>
					</div>
				<?php else: ?>	
                    <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 search-results_background">
                        Brak wyników wyszukiwania.
                    </div>			
				<?php endif; ?>
			</div>
    	</div>
	    <div class="col-lr-0 col-lg-3 col-md-3 col-sm-12 col-xs-12 taxonomy-left pull-left">
	        <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 taxonomy-left__color">                
	            <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
	                <div id="searchbox">
	                    <span class="search-title">Wyszukaj</span>
                        <?php get_product_search_form(); ?>
	                </div>
	            </div>
	            <div class="col-lr-0 col-lg-12 col-md-12 hidden-sm hidden-xs ">
	                <a href="/produkty/">Kategorie produktów</a>
	                <ul class="product-category">
	                    <?php $terms = get_terms(array('taxonomy' => 'produkty-category','orderby' => 'menu_order','order' => 'desc','hide_empty' => false)); ?>
	                    <?php foreach ( $terms as $term ) { ?>
	                        <?php $this_category = get_the_terms(get_the_ID(),'produkty-category'); ?>
	                        <li <?php if($tax->slug == $term->slug)   { echo 'class="active"'; } ?>>
	                            <a href="/produkty/<?php echo $term->slug; ?>" class="category_url">
	                            <?php echo $term->name; ?>
	                        </a></li>
	                    <?php } ?>
	                </ul>
	            </div>
	            <div class="col-lr-0 hidden-lg hidden-md col-sm-12 col-xs-12">
	                <div class="col-lr-0 category-title">
	                    <a href="/produkty/">Kategorie produktów</a>
	                </div>
	                <div class="col-lr-0 nav-container">
	                    <div class="form-group">
	                        <select class="form-control nav" onchange="location = this.value;">
	                            <?php $terms = get_terms(array('taxonomy' => 'produkty-category','orderby' => 'menu_order','order' => 'desc','hide_empty' => false)); ?>
	                            <?php foreach ( $terms as $term ) { ?>
	                                <?php $this_category = get_the_terms(get_the_ID(),'produkty-category'); ?>
	                                    <option <?php if($tax->slug == $term->slug)   { echo 'class="active"'; } ?> value="/produkty/<?php echo $term->slug; ?>" class="category_url">
	                                        <?php echo $term->name; ?>
	                                    </option>
	                            <?php } ?>
	                        </select>
	                    </div>
	                 </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>







	<!-- OLD



	<div class="container background-white">		
		<div style="padding:15px;">
			Wyniki wyszukiwania dla: <strong><?php echo get_search_query(); ?></strong> <br /><br />					
				
			<?php  if (have_posts()): ?>
			<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">	
					<?php $i = 1; ?>
					<?php while(have_posts()): the_post(); ?>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="color:#000;">
					
						<a style="color:#000;" href="<?php the_permalink(); ?>">
						<?php if ( has_post_thumbnail() ) { ?>
							<div style="text-align: center;" class="search-thumb">
								<img style="margin: 0 auto;" src="<?php the_post_thumbnail_url('medium'); ?>" class="search-thumb img-responsive" >
								<?php the_title(); ?>
							</div>
						<?php } else { ?>
							<div style="text-align: center;" class="search-thumb">
								<img style="margin: 0 auto;" src="<?php echo get_template_directory_uri(); ?>/img/brak_zdjecia.jpg" class="search-thumb img-responsive">
								<?php the_title(); ?>
							</div>
						<?php } ?>
						</a>					
					</div>
					
					<?php if(($i%4) == 0) { echo '<div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">&nbsp;<br /></div></div>'; } ?>
					
					<?php $i++; ?>
					
					<?php endwhile; ?>
				</div>
			<?php else: ?>
			
									
				Brak wyników wyszukiwania.
									
			<?php endif; ?>
		</div>
		<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-more">
				<a href="#" onclick="window.history.go(-1); return false;">« Wróć</a>
			</div>	
		<br /><br /><br /><br />
		
	</div> -->

<?php get_footer(); ?>