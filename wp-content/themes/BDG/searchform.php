<?php $currentlang = get_bloginfo('language'); ?>
<form id="searchform" class="form-inline" role="search" method="get" action="<?php bloginfo('url'); ?>/">
	<div class="form-group">
		<?php if($currentlang=="pl-PL") { ?>
		<input onclick="ga('send', 'event', 'wyszukiwarka', 'kliknięcie', 'rozwinięcie');" type="search" class="searchform" placeholder="Wyszukaj..." value="<?php the_search_query(); ?>" name="s" id="s" />
		<?php } ?>
        <input type="hidden" name="post_type" value="product" />
		<?php if($currentlang=="en-US") { ?>
		<input onclick="ga('send', 'event', 'wyszukiwarka', 'kliknięcie', 'rozwinięcie');" type="search" class="searchform" placeholder="Search..." value="<?php the_search_query(); ?>" name="s" id="s" />
		<?php } ?>
	</div>
</form>