<?php
/*
    Template Name: Sklep
*/
?>
<?php
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language');
?>
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('<?php echo get_field('top_banner'); ?>');">
    <div class="caption-over-block-all">
        <div class="caption-over-outer-all">
            <div class="caption-over-inner-all top-banner-padding">
                <div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
                    <h1><?php the_title(); ?></h1>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down-white.png" class="img-responsive top-banner-twig">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 products-home" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.jpg');">
    <div class="container">
        <?php wc_get_template_part( 'content', 'product' ); ?>
    </div>
</div>
<?php get_footer(); ?>