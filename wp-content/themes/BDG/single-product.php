<?php 
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); 
?>
<?php
$terms = get_the_terms( $post->ID, 'product_cat' );
foreach ( $terms as $term ) {
    $product_cat_id = $term->term_id;
    $product_parent_categories_all_hierachy = get_ancestors( $product_cat_id, 'product_cat' );

    $last_parent_cat = array_slice($product_parent_categories_all_hierachy, -1, 1, true);
    foreach($last_parent_cat as $last_parent_cat_value){
        $top_cat_id = $last_parent_cat_value;
    }
    if( $term = get_term_by( 'id', $product_cat_id, 'product_cat' ) ){
        $this_category_name = $term->name;
    }
    break;
}
?>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('<?php echo get_field('top_banner', 'product_cat_'.$top_cat_id); ?>');">
    <div class="caption-over-block-all">
        <div class="caption-over-outer-all">
            <div class="caption-over-inner-all top-banner-padding">
                <div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
                    <h1>Kategoria: <?php echo $this_category_name; ?></h1>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down-white.png" class="img-responsive top-banner-twig">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 products-home product" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.jpg');">
    <img src="<?php echo get_template_directory_uri(); ?>/img/twig-promo-down.png" class="img-responsive twig-promo-down-products">
    <div class="col-lr-0 container">
            <?php echo get_field('tekst_wprowadzajacy'); ?>
        <div class="col-lr-0 col-lg-9 col-md-9 col-sm-12 col-xs-12 taxonomy-right-single  pull-right">
            <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 product-back product-padding">
                <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php woocommerce_show_product_images(); ?>
                    <div class="summary entry-summary">
                        <?php
                        /**
                         * Hook: woocommerce_single_product_summary.
                         *
                         * @hooked woocommerce_template_single_title - 5
                         * @hooked woocommerce_template_single_rating - 10
                         * @hooked woocommerce_template_single_price - 10
                         * @hooked woocommerce_template_single_excerpt - 20
                         * @hooked woocommerce_template_single_add_to_cart - 30
                         * @hooked woocommerce_template_single_meta - 40
                         * @hooked woocommerce_template_single_sharing - 50
                         * @hooked WC_Structured_Data::generate_product_data() - 60
                         */
                        do_action( 'woocommerce_single_product_summary' );
                        ?>
                    </div>
                </div>
                <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php
                    /**
                     * Hook: woocommerce_after_single_product_summary.
                     *
                     * @hooked woocommerce_output_product_data_tabs - 10
                     * @hooked woocommerce_upsell_display - 15
                     * @hooked woocommerce_output_related_products - 20
                     */
                    do_action( 'woocommerce_after_single_product_summary' );
                    ?>
                </div>
            
            <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-more">
                <a href="#" onclick="window.history.go(-1); return false;">« Wróć</a>
            </div>  
            </div>
        </div>  
        <div class="col-lr-0 col-lg-3 col-md-3 col-sm-12 col-xs-12 taxonomy-left pull-left">
            <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 taxonomy-left__color">
                <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <div id="searchbox">
                        <span class="search-title">Wyszukaj</span>
                        <?php get_product_search_form(); ?>
                    </div>
                </div>
                <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <?php if ( ! dynamic_sidebar( 'woo-filters' ) ) : ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>  
    </div>
</div>

<script>
$(document).ready(function(){
    $('#foto a').fancybox({
        thumbs : {
            autoStart   : false,
            hideOnClose : true
        }
    });
});
</script>

        
<?php 
    $value = get_field('wyswietlenia_wpisu') + 1;
    update_field('wyswietlenia_wpisu', $value );
?>

<?php get_footer(); ?>
