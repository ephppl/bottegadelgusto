<?php 
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); 
?>
<?php 
	$this_category = get_the_terms(get_the_ID(),'produkty-category'); 
	$this_category_id = $this_category[0]->term_taxonomy_id;
	$this_category_name = $this_category[0]->name;
?>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('<?php echo get_field('top_banner', 'term_'.$this_category_id); ?>');">
	<div class="caption-over-block-all">
		<div class="caption-over-outer-all">
			<div class="caption-over-inner-all top-banner-padding">
				<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
					<h1>Kategoria: <?php echo $this_category_name; ?></h1>
					<img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down-white.png" class="img-responsive top-banner-twig">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 products-home" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.jpg');">
	<img src="<?php echo get_template_directory_uri(); ?>/img/twig-promo-down.png" class="img-responsive twig-promo-down-products">
	<div class="col-lr-0 container">
		<!--<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 center">
			<?php echo get_field('tekst_wprowadzajacy'); ?>
		</div>-->
		
		
		
		<div class="col-lr-0 col-lg-9 col-md-9 col-sm-12 col-xs-12 taxonomy-right-single  pull-right">
			<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 product-back product-padding">
			    <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div id="foto" class="col-lr-0 col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<?php if(!empty(get_the_post_thumbnail_url(get_the_ID()))) { ?>
					    <a href="<?php the_post_thumbnail_url('large'); ?>"><img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'medium'); ?>" class="img-responsive" style="margin: 0 auto;"></a>
					<?php } else { ?>
					    <img src="<?php echo get_template_directory_uri(); ?>/img/brak_zdjecia.jpg" class="img-responsive">
					<?php } ?>
					
				</div>
				<div class="col-lr-0 col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<h2 style="text-transform: capitalize;"><?php the_title(); ?></h2>
					<?php if(get_field('cena', $post->ID)) { ?>
						Cena:<br />
						<div class="product-price-single"><?php echo get_field('cena', $post->ID); ?></div>
					<?php } ?>
				</div>
			</div>
			
			<?php if(!empty(get_the_content())) { ?>
				<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h2 style="text-transform: capitalize;">Opis:</h2>
				<?php the_content(); ?>
				</div>
			<?php } ?>
			
			
			
			<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-more">
				<a href="#" onclick="window.history.go(-1); return false;">« Wróć</a>
			</div>	
			</div>
		</div>	
		<div class="col-lr-0 col-lg-3 col-md-3 col-sm-12 col-xs-12 taxonomy-left pull-left">
            <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 taxonomy-left__color">                
                <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <div id="searchbox">
                        <span class="search-title">Wyszukaj</span>
                        <?php get_product_search_form(); ?>
                    </div>
                </div>
                <div class="col-lr-0 col-lg-12 col-md-12 hidden-sm hidden-xs ">
                    <a href="/produkty/">Kategorie produktów</a>
                    <ul class="product-category">
                        <?php $terms = get_terms(array('taxonomy' => 'produkty-category','orderby' => 'menu_order','order' => 'desc','hide_empty' => false)); ?>
                        <?php foreach ( $terms as $term ) { ?>
                            <?php $this_category = get_the_terms(get_the_ID(),'produkty-category'); ?>
                            <li <?php if($tax->slug == $term->slug)   { echo 'class="active"'; } ?>>
                                <a href="/produkty/<?php echo $term->slug; ?>" class="category_url">
                                <?php echo $term->name; ?>
                            </a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="col-lr-0 hidden-lg hidden-md col-sm-12 col-xs-12">
                    <div class="col-lr-0 category-title">
                        <a href="/produkty/">Kategorie produktów</a>
                    </div>
                    <div class="col-lr-0 nav-container">
                        <div class="form-group">
                            <select class="form-control nav" onchange="location = this.value;">
                                <?php $terms = get_terms(array('taxonomy' => 'produkty-category','orderby' => 'menu_order','order' => 'desc','hide_empty' => false)); ?>
                                <?php foreach ( $terms as $term ) { ?>
                                    <?php $this_category = get_the_terms(get_the_ID(),'produkty-category'); ?>
                                        <option <?php if($tax->slug == $term->slug)   { echo 'class="active"'; } ?> value="/produkty/<?php echo $term->slug; ?>" class="category_url">
                                            <?php echo $term->name; ?>
                                        </option>
                                <?php } ?>
                            </select>
                        </div>
                     </div>
                </div>
            </div>
        </div>	
	</div>
</div>

<script>
$(document).ready(function(){
	$('#foto a').fancybox({
		thumbs : {
			autoStart   : false,
			hideOnClose : true
		}
	});
});
</script>

		
<?php 
	$value = get_field('wyswietlenia_wpisu') + 1;
	update_field('wyswietlenia_wpisu', $value );
?>

<?php get_footer(); ?>
