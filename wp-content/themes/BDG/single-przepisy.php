<?php 
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); 
?>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/przepisy.jpg');">
	<div class="caption-over-block-all">
		<div class="caption-over-outer-all">
			<div class="caption-over-inner-all top-banner-padding">
				<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
					<h1><?php the_title(); ?></h1>
					<img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down-white.png" class="img-responsive top-banner-twig">
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 recipes-home" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.jpg');">
	<div class="col-lr-0 container background-white">
		<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-big">
				<div class="col-lr-0 col-lg-3 col-md-3 col-sm-3 col-xs-12 przepisy-category pull-right">
					<?php get_template_part( 'przepisy-kolumna' ); ?>
				</div>
				<div class="col-l-0 col-lg-9 col-md-9 col-sm-9 col-xs-12 pull-left">
					<?php 
						$image_news = get_field('zdjecie_przepisu'); 
						if($image_news!="") {
					?>	
					<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<?php 
						$words = explode(' ', get_the_title());
						$words_all = $words;
						array_shift($words);
						$variable = implode(' ', $words);
						 ?>
						<div class="przepisy-category-title"><?php $categories = wp_get_object_terms(get_the_ID(), 'przepisy-category'); echo $categories[0]->name; ?></div>
						<?php 
							$image_news = get_field('zdjecie_przepisu'); 
							if($image_news!="") { echo '<img src="'.$image_news['url'].'" class="img-responsive"/>'; }
						?>	
						<div class="przepisy-title"><strong><?php echo $words_all[0]; ?></strong> <span><?php echo $variable; ?></span></div>					
					</div>	
					
					
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">		
						<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-content">
							<span class="przepisy-date"><?php echo the_date(); unset($previousday); ?></span>

							<?php the_content(); ?>
							
							<?php 
							$custom_post_tags = wp_get_object_terms($post->ID, array('przepisy-tagi') );
							
							if(!empty($custom_post_tags)) {
								if(!is_wp_error($custom_post_tags)) {
									echo 'Tagi: ';
									$j=1;
									foreach($custom_post_tags as $term) {
										if($j!=1) { echo ', '; };
										echo '<a href="' . get_term_link( $term->slug, 'przepisy-tagi' ) . '">' . esc_html( $term->name ) . '</a>'; 
										$j++;
									}
								}
							}
							
							$skladniki = get_field('skladniki');
							
							if(!empty($skladniki)) { ?>
								<strong>Składniki</strong>
								<?php
								echo $skladniki;
							}
							
							$przygotowanie = get_field('przygotowanie');
							
							if(!empty($przygotowanie)) { ?>
								<strong>Sposób przygotowania</strong>
								<?php
								echo $przygotowanie;
							}
							?>
						</div>
					</div>		
					<?php } else { ?>
						<?php the_excerpt(); ?>
					<?php } ?>
			
					<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-more">
						<a href="#" onclick="window.history.go(-1); return false;">« Wróć</a>
					</div>

				</div>
		</div>
	</div>
</div>		
<?php 
	$value = get_field('wyswietlenia_wpisu') + 1;
	update_field('wyswietlenia_wpisu', $value );
?>

<?php get_footer(); ?>
