<?php get_header(); $currentlang = get_bloginfo('language'); $category_post = get_the_category(get_the_ID());  ?>
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('/wp-content/uploads/2017/09/aktualnosci-background.jpg');">
	<div class="caption-over-block-all">
		<div class="caption-over-outer-all">
			<div class="caption-over-inner-all top-banner-padding">
				<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
					<h1><?php single_cat_title(); ?></h1>
					<img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down-white.png" class="img-responsive top-banner-twig">
				</div>
			</div>
		</div>
	</div>
</div>
<style>
	.news-content a {
		color:#000;
		text-decoration: underline;
	}
</style>
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 recipes-home-przepisy" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.jpg');">
	<div class="col-lr-0 container">
		<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 background-white">
			<?php 
$not_show_grafika = get_field('nie_pokazuj_na_podstronie');
					$grafikaaktualnosci = get_field('grafika'); 
					$trescaktualnosci = get_the_content(); 
					$tytułaktualnosci = get_the_title();
				?>
			<?php if(!$not_show_grafika): ?><div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<img src="<?php echo $grafikaaktualnosci; ?>" class="img-responsive">
			</div><?php endif; ?>
			<div class="news-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<span class="aktualnosci-title"><?php echo $tytułaktualnosci;?></span>
				<?php the_content(); //echo $trescaktualnosci; ?>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-more">
				<a href="#" onclick="window.history.go(-1); return false;">« Wróć</a>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>