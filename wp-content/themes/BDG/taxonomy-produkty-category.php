<?php 
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); ?>

<?php /*Pobiera aktualny taxonomy*/ $tax = $wp_query->get_queried_object(); ?>

<?php 
	if($tax->slug == 'wina' && !isset($_GET['a'])) {
		header('Location: http://bottegadelgusto.pl/wina');
		exit();
	}
	else {
?>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('http://bottegadelgusto.pl/wp-content/themes/BDG/img/produkty.jpg');">
    <div class="caption-over-block-all">
        <div class="caption-over-outer-all">
            <div class="caption-over-inner-all top-banner-padding">
                <div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
                    <h1 class="banner-title">
                        <?php echo $tax->name; ?></h1>
                    <img src="http://bottegadelgusto.pl/wp-content/themes/BDG/img/twig-slider-down-white.png" class="img-responsive top-banner-twig">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 products-home" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.jpg');">
	<img src="<?php echo get_template_directory_uri(); ?>/img/twig-promo-down.png" class="img-responsive twig-promo-down-products">
	<div class="col-lr-0 container">	
		<div class="col-lr-0 col-lg-9 col-md-9 col-sm-12 col-xs-12 taxonomy-right pull-right">
			<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
			<?php 		
					$args = array(
						'posts_per_page' => -1,
						'post_type' => 'produkty',
						'orderby' => 'title',
						'order' => 'asc',
						'tax_query' => array(
							array(
								'taxonomy' => 'produkty-category',
								'field'    => 'slug',
								'terms'    => $tax->slug,
							),
						),
					);
				@$myquery = new WP_Query( $args );				
					$i = 1; 
                    $count = $myquery->post_count; 
                    $liczbaPostow = $count;

                    if($liczbaPostow >= 1) { ?>
                        <?php 
                        while ($myquery->have_posts()) {                          
                        $myquery->the_post();
                        $all_meta = get_post_meta(get_the_ID());
                        @$galeria = get_field('galeria');
                        $cena = get_field('cena');      
                         ?>
                           
                        <?php if(($i%3) == 1 ) { ?>
                            <div class="row row-eq">
                        <?php } ?>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-eq">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-box category-box-padding">
                            <a class="product-link" href="<?php echo get_permalink();?>">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-image">

                                    <?php if(!empty(get_the_post_thumbnail_url(get_the_ID(),'medium'))) { ?>
                                        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'medium'); ?>" class="img-responsive">
                                    <?php } else { ?>
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/brak_zdjecia.jpg" class="img-responsive">
                                    <?php } ?>
                                </div>
                            </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-title"><?php echo get_the_title(); ?></div>
                                <?php // if(get_field('cena', $post->ID)) { ?>
                                     <!--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-price"><?php echo $cena; ?></div>-->
                                <?php // } ?>
                            </div>
                        </div>
                        <?php if(($i%3) == 0 && $i != 0) { ?>
                            </div>
                        <?php } elseif ($i === $liczbaPostow) { ?>
                            </div>
                        <?php } ?>
                    <?php
                    $i++; 
                    }   
                    ?>
                    <?php } else { ?>
                        <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 search-results_background">
                            Brak wyników wyszukiwania.
                        </div>  
                    <?php } ?>
			</div>
		</div>
        <div class="col-lr-0 col-lg-3 col-md-3 col-sm-12 col-xs-12 taxonomy-left pull-left">
            <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 taxonomy-left__color">                
                <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <div id="searchbox">
                        <span class="search-title">Wyszukaj</span>
                        <?php get_product_search_form(); ?>
                    </div>
                </div>
                <div class="col-lr-0 col-lg-12 col-md-12 hidden-sm hidden-xs ">
                    <a href="/produkty/">Kategorie produktów</a>
                    <ul class="product-category">
                        <?php $terms = get_terms(array('taxonomy' => 'produkty-category','orderby' => 'menu_order','order' => 'desc','hide_empty' => false)); ?>
                        <?php foreach ( $terms as $term ) { ?>
                            <?php $this_category = get_the_terms(get_the_ID(),'produkty-category'); ?>
                            <li <?php if($tax->slug == $term->slug)   { echo 'class="active"'; } ?>>
                                <a href="/produkty/<?php echo $term->slug; ?>" class="category_url">
                                <?php echo $term->name; ?>
                            </a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="col-lr-0 hidden-lg hidden-md col-sm-12 col-xs-12">
                    <div class="col-lr-0 category-title">
                        <a href="/produkty/">Kategorie produktów</a>
                    </div>
                    <div class="col-lr-0 nav-container">
                        <div class="form-group">
                            <select class="form-control nav" onchange="location = this.value;">
                                <?php $terms = get_terms(array('taxonomy' => 'produkty-category','orderby' => 'menu_order','order' => 'desc','hide_empty' => false)); ?>
                                <?php foreach ( $terms as $term ) { ?>
                                    <?php $this_category = get_the_terms(get_the_ID(),'produkty-category'); ?>
                                        <option <?php if($tax->slug == $term->slug)   { echo 'class="active"'; } ?> value="/produkty/<?php echo $term->slug; ?>" class="category_url">
                                            <?php echo $term->name; ?>
                                        </option>
                                <?php } ?>
                            </select>
                        </div>
                     </div>
                </div>
            </div>
        </div>
	</div>
</div>

<?php } ?>

<?php get_footer(); ?>