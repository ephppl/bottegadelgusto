<?php
get_header(); setup_postdata($post); $currentlang = get_bloginfo('language'); 
?>
<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 top-banner" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/przepisy.jpg');">
	<div class="caption-over-block-all">
		<div class="caption-over-outer-all">
			<div class="caption-over-inner-all top-banner-padding">
				<div class="col-lr-0 col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 page-title">
					<h1>Kategoria: <?php echo single_cat_title("", false); ?></h1>
					<img src="<?php echo get_template_directory_uri(); ?>/img/twig-slider-down-white.png" class="img-responsive top-banner-twig">
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 recipes-home" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/products-bg-home.jpg');">
	<div class="col-lr-0 container background-white">
		<?php $i=1; while ( have_posts() ) : the_post(); ?>
			<?php if($i==1) { ?> 
				<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-big">
					<div class="col-lr-0 container">
						<div class="col-lr-0 col-lg-3 col-md-3 col-sm-3 col-xs-12 przepisy-category pull-right">
							<?php get_template_part( 'przepisy-kolumna' ); ?>
						</div>
						<div class="col-l-0 col-lg-9 col-md-9 col-sm-9 col-xs-12">
							<?php 
								$image_news = get_field('zdjecie_przepisu'); 
								if($image_news!="") {
							?>
							<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-hover pull-left">
								<a href="<?php the_permalink(); ?>">
									<?php 
									$words = explode(' ', get_the_title());
									$words_all = $words;
									array_shift($words);
									$variable = implode(' ', $words);
									 ?>
									<div class="przepisy-category-title"><?php $categories = wp_get_object_terms(get_the_ID(), 'przepisy-category'); echo $categories[0]->name; ?></div>
									<img src="<?php echo $image_news['url']; ?>" class="img-responsive"/>
									<div class="przepisy-title"><strong><?php echo $words_all[0]; ?></strong> <span><?php echo $variable; ?></span></div>	
								</a>
							</div>	
							<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">		
								<div class="row table-row-xs">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-content">
										<span class="przepisy-date"><?php echo the_date(); unset($previousday); ?></span>
										<?php the_excerpt(); ?>
										<?php 
										$custom_post_tags = wp_get_object_terms($post->ID, array('tagi') );
										
										if(!empty($custom_post_tags)) {
											if(!is_wp_error($custom_post_tags)) {
												echo '<strong>Tagi: </strong>';
												$j=1;
												foreach($custom_post_tags as $term) {
													if($j!=1) { echo ', '; };
													echo '<a href="' . get_term_link( $term->slug, 'tagi' ) . '">' . esc_html( $term->name ) . '</a>'; 
													$j++;
												}
											}
										}
										?>
										<div class="col-lr-0 col-lg-12 col-md-12 col-sm-4 col-xs-12 przepisy-more">
											<a href="<?php the_permalink(); ?>">
												Więcej »
											</a>
										</div>
									</div>
								</div>
							</div>		
							<?php } else { ?>
								<?php the_excerpt(); ?>
							<?php } ?>
								
							<a href="<?php the_permalink(); ?>"><div class="more"></div></a>
						</div>
					</div>
				</div>
				<div class="col-lr-0 container">
					<div class="col-l-0 col-lg-9 col-md-9 col-sm-12 col-xs-12">
			<?php } else { ?>
					<div class="<?php if($i==2) { echo 'col-l-0 '; } else { echo 'col-r-0 '; } ?>col-lg-6 col-md-6 col-sm-6 col-xs-12 przepisy-small ">
						<?php 
							$image_news = get_field('zdjecie_przepisu'); 
							if($image_news!="") {
						?>
						<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-hover">
							<a href="<?php the_permalink(); ?>">
								<?php 
								$words = explode(' ', get_the_title());
								$words_all = $words;
								array_shift($words);
								$variable = implode(' ', $words);
								 ?>
								<div class="przepisy-category-title"><?php $categories = wp_get_object_terms(get_the_ID(), 'przepisy-category'); echo $categories[0]->name; ?></div>
								<img src="<?php echo $image_news['url']; ?>" class="img-responsive"/>
								<div class="przepisy-title"><strong><?php echo $words_all[0]; ?></strong> <span><?php echo $variable; ?></span></div>	
							</a>
						</div>	
						<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-hover">		
							<div class="row table-row-xs">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-content">
									<span class="przepisy-date"><?php echo the_date(); unset($previousday); ?></span>
									<?php the_excerpt(); ?>
									<?php 
									$custom_post_tags = wp_get_object_terms($post->ID, array('tagi') );
									
									if(!empty($custom_post_tags)) {
										if(!is_wp_error($custom_post_tags)) {
											echo '<strong>Tagi: </strong>';
											$j=1;
											foreach($custom_post_tags as $term) {
												if($j!=1) { echo ', '; };
												echo '<a href="' . get_term_link( $term->slug, 'tagi' ) . '">' . esc_html( $term->name ) . '</a>'; 
												$j++;
											}
										}
									}
									?>
									<div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 przepisy-more">
										<a href="<?php the_permalink(); ?>">
											Więcej »
										</a>
									</div>
								</div>
							</div>
						</div>		
						<?php } else { ?>
							<?php the_excerpt(); ?>
						<?php } ?>
							
						<a href="<?php the_permalink(); ?>"><div class="more"></div></a>
					</div>
			<?php } ?>
		<?php $i++; endwhile; ?>
				</div>
				<div class="col-lr-0 col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
				
				<div class="col-l-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
					<?php wpbeginner_numeric_posts_nav(); ?>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>