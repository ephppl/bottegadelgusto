<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>

<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-eq-2">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-box category-box-padding">
        <a class="product-link" href="<?php the_permalink(); ?>">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-image">
                <?php echo $product->get_image(); ?>
            </div>
        </a>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-title"><?php echo $product->name; ?></div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-price"><?php
            if($product->get_regular_price()){
                if( $product->is_on_sale() ) {
                    echo '<p class="product-price sale">'.$product->get_sale_price().' zł'.get_post_meta(get_the_ID(), '_mcmp_ppu_recalc_text_override', true).'</p>';
                    echo '<p class="product-price"><small><del>'.$product->get_regular_price().' zł'.get_post_meta(get_the_ID(), '_mcmp_ppu_recalc_text_override', true).'</del></small></p>';
                }
                else{
                    echo '<p class="product-price">'.number_format($product->get_regular_price(),2,'.','').' zł'.get_post_meta(get_the_ID(), '_mcmp_ppu_recalc_text_override', true).'</p>';
                }
            }
        ?></div>
        <?php
        /**
         * Hook: woocommerce_after_shop_loop_item.
         *
         * @hooked woocommerce_template_loop_product_link_close - 5
         * @hooked woocommerce_template_loop_add_to_cart - 10
         */
        do_action( 'woocommerce_after_shop_loop_item' );
        ?>
    </div>
</div>