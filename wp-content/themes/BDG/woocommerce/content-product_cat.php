<?php
/**
 * The template for displaying product category thumbnails within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$img = get_field('miniaturka_na_stronie_glownej', $category);
if(!$img) {
    $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
    $img = wp_get_attachment_url( $thumbnail_id );
} 
?>

<a href="<?php echo get_term_link( $category->term_id, 'product_cat' ); ?>" title="<?php echo $category->name; ?>">
    <div class="col-lr-0 col-lg-3 col-md-4 col-sm-6 col-xs-12 h-lg-3 h-md-4 h-sm-6 h-xs-12">
        <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 category-box" style="background-image: url('<?php echo $img; ?>');">
            <div id="parent-<?php echo $category->term_id; ?>" class="parent-page center h-content">
                <div class="caption-over-block-all">
                    <div class="caption-over-outer-all">
                        <div class="caption-over-inner-all">
                            <h1><?php
                                echo $category->name;
                                ?></h1>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/twig-products.png" class="img-responsive twig-products">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</a>
