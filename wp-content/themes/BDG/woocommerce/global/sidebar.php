<?php
/**
 * Sidebar
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/sidebar.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
if ( is_product_category() or is_product_taxonomy() or is_product() ) {
    $cTerm = get_queried_object();
    ?>
    <div class="col-lr-0 col-lg-3 col-md-3 col-sm-12 col-xs-12 taxonomy-left pull-left">
        <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 taxonomy-left__color">
            <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div id="searchbox">
                    <span class="search-title">Wyszukaj</span>
                    <?php get_product_search_form(); ?>
                </div>
            </div>
            <?php if(is_product_category() or is_product_taxonomy()): ?>
                <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <?php if ( ! dynamic_sidebar( 'woo-filters' ) ) : ?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <?php
}

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
