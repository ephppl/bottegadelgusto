<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="products-wrap">
    <?php if(is_shop()): ?>
    <a href="<?php echo get_home_url(); ?>/e-sklep/wszystkie-produkty" title="Wszystkie produkty">
        <div class="col-lr-0 col-lg-3 col-md-4 col-sm-6 col-xs-12 h-lg-3 h-md-4 h-sm-6 h-xs-12">
            <div class="col-lr-0 col-lg-12 col-md-12 col-sm-12 col-xs-12 category-box" style="background-image: url('');">
                <div id="parent-35" class="parent-page center h-content">
                    <div class="caption-over-block-all">
                        <div class="caption-over-outer-all">
                            <div class="caption-over-inner-all">
                                <h1>Wszystkie produkty</h1>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/twig-products.png" class="img-responsive twig-products">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </a>
    <?php endif; ?>